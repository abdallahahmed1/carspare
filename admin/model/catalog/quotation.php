<?php

class ModelCatalogQuotation extends Model
{
    public function add($data)
    {

        $q = "insert into cs_quotations set name = '" . $this->db->escape($data['quote_name'])."'";
        $this->db->query($q);

        $quotation_id = $this->db->getLastId();

        if (isset($data['products'])) {
            foreach ($data['products'] as $product) {
                $this->db->query("Insert into cs_quotations_products set quotation_id =  $quotation_id, product_id = ".(int)$product);
            }
        }
    }

    public function edit($data)
    {

        $quotation_id = $this->request->get['quotation_id'];

        $q = "update cs_quotations set name = '" . $this->db->escape($data['quote_name']) . "' where quotation_id = $quotation_id";

        $this->db->query($q);

        if (isset($data['products'])) {
            $this->db->query("delete from cs_quotations_products where quotation_id = " . (int)$quotation_id);
            foreach ($data['products'] as $product) {
                $this->db->query("Insert into cs_quotations_products set quotation_id =  $quotation_id, product_id = ".$product);
            }
        }
    }

    public function delete($quotation_id)
    {
        $this->db->query("delete from cs_quotations where quotation_id = $quotation_id");
        $this->db->query("delete from cs_quotations_products where quotation_id = " . (int)$quotation_id);
    }

    public function getQuote($id){
        $result = $this->db->query("select * from cs_quotations where quotation_id = ".(int)$id)->rows;
        $quote = $result[0];

        return array('name' => $quote['name'], 'quotation_id' => $quote['quotation_id']);
    }

    public function getQuotes()
    {
        $results = $this->db->query("select * from cs_quotations ")->rows;
        $quotes = array();
        foreach ($results as $result){
            $quotes[] = array(
                'name' => $result['name'],
                'quotation_id' =>$result['quotation_id']
            );
        }
        return $quotes;
    }

    public function getQuoteProducts($quotation_id)
    {
        $results = $this->db->query("select * from cs_quotations_products where quotation_id = ".(int)$quotation_id)->rows;
        $products = array();
        foreach ($results as $result){
            $products[] = $result['product_id'];
        }
        return $products;
    }

}
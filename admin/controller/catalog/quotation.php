<?php

class ControllerCatalogQuotation extends Controller
{
    public $error = array();

    public function index()
    {
        $this->document->setTitle("Quotations");

        $this->getList();
    }

    public function getList()
    {

        $this->load->model('catalog/quotation');

        $data = array();

        $quotations = $this->model_catalog_quotation->getQuotes();
        foreach ($quotations as $key => $quote) {
            $quotations[$key]['edit'] = $this->url->link('catalog/quotation/edit', 'user_token=' . $this->session->data['user_token'] . '&quotation_id=' . $quote['quotation_id'], true);
        }
        $data['quotations'] = $quotations;

        $data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => "Home",
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => "Quotations",
            'href' => $this->url->link('catalog/quotation', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['add'] = $this->url->link('catalog/quotation/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['copy'] = $this->url->link('catalog/quotation/copy', 'user_token=' . $this->session->data['user_token'], true);
        $data['delete'] = $this->url->link('catalog/quotation/delete', 'user_token=' . $this->session->data['user_token'], true);
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/quotations', $data));
    }

    public function getForm()
    {

        $this->load->model('catalog/quotation');
        $this->load->model('catalog/product');

        $data = array();

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        }

        if (isset($this->error['products'])) {
            $data['error_products'] = $this->error['products'];
        }

        if (isset($this->request->get['quotation_id']) ||
            isset($this->request->post['quotation_id'])) {
            $id = isset($this->request->get['quotation_id']) ? $this->request->get['quotation_id'] : $this->request->post['product_id'];
            $quotation_info = $this->model_catalog_quotation->getQuote($id);
        }


        if (isset($this->request->post['quote_name'])) {
            $data['quote_name'] = $this->request->post['quote_name'];
        } elseif (!empty($quotation_info)) {
            $data['quote_name'] = $quotation_info['name'];
        } else {
            $data['quote_name'] = '';
        }

        $data['products'] = array();

        if (isset($this->request->post['products'])) {
            foreach ($this->request->post['products'] as $product) {

                $data['products'][] = array(
                    'product_id' => $product,
                    'name' => $product ? $this->model_catalog_product->getProduct($product)['name'] : ''
                );
            }
        } elseif (!empty($quotation_info)) {
            $ids = $this->model_catalog_quotation->getQuoteProducts($quotation_info['quotation_id']);
            foreach ($ids as $product) {
                $data['products'][] = array(
                    'product_id' => $product,
                    'name' => $this->model_catalog_product->getProduct($product)['name']
                );

            }
        } else {
            $data['products'] = null;
        }
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => "Home",
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => "Quotations",
            'href' => $this->url->link('catalog/quotation', 'user_token=' . $this->session->data['user_token'], true)
        );

        if (!empty($quotation_info)) {
            $data['action'] = $this->url->link('catalog/quotation/edit', 'user_token=' . $this->session->data['user_token']."&quotation_id=".$quotation_info['quotation_id'],true);
        }else{
            $data['action'] = $this->url->link('catalog/quotation/add', 'user_token=' . $this->session->data['user_token'], true);
        }
        $data['delete'] = $this->url->link('catalog/quotation/delete', 'user_token=' . $this->session->data['user_token'], true);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/quotation_form', $data));
    }

    public function add()
    {
        $this->load->model('catalog/quotation');
        if ($this->request->server['REQUEST_METHOD'] == "POST" && $this->validate()) {
            $this->model_catalog_quotation->add($this->request->post);
            $this->session->data['success'] = "success";
            $this->getList();
        } else {
            $this->getForm();
        }
    }

    public function edit()
    {
        $this->load->model('catalog/quotation');

        if ($this->request->server['REQUEST_METHOD'] == "POST" && $this->validate()) {
            $this->model_catalog_quotation->edit($this->request->post);
            $this->session->data['success'] = "Success";
            $this->getList();
        } else {

            $this->getForm();
        }
    }

    public function delete()
    {
        $this->load->model('catalog/quotation');
        foreach ($this->request->post['selected'] as $product_id) {
            $this->model_catalog_quotation->delete($product_id);
        }
        $this->getList();
    }

    public function validate()
    {
        if (isset($this->request->post['name']) && strlen($this->request->post['name']) < 1) {
            $this->error['name'] = 'Enter Number';
        }

        if (isset($this->request->post['products'])) {
            foreach ($this->request->post['products'] as $product) {
                if (empty($product)) {
                    $this->error['products'] = 'Choose products';
                    break;
                }
            }
        }
        return !$this->error;
    }

    public function autoComplete()
    {

    }
}
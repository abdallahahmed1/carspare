<?php

class ControllerCatalogCar extends Controller {
    private $error = array();
    
    public function index() {
		$this->load->language('catalog/car');

		$this->document->setTitle($this->language->get('heading_title'));

		// $this->load->model('catalog/car');

		$this->getList();
    }
    
    public function add(){

        $this->load->language('catalog/car');

        $this->document->setTitle($this->language->get('heading_title'));

        // $this->load->model('catalog/car');

        // if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		// 	$this->model_catalog_product->addProduct($this->request->post);

		// 	$this->session->data['success'] = $this->language->get('text_success');

		// 	$url = '';

		// 	if (isset($this->request->get['filter_name'])) {
		// 		$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		// 	}

		// 	if (isset($this->request->get['filter_model'])) {
		// 		$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		// 	}

		// 	if (isset($this->request->get['filter_price'])) {
		// 		$url .= '&filter_price=' . $this->request->get['filter_price'];
		// 	}

		// 	if (isset($this->request->get['filter_quantity'])) {
		// 		$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		// 	}

		// 	if (isset($this->request->get['filter_status'])) {
		// 		$url .= '&filter_status=' . $this->request->get['filter_status'];
		// 	}

		// 	if (isset($this->request->get['sort'])) {
		// 		$url .= '&sort=' . $this->request->get['sort'];
		// 	}

		// 	if (isset($this->request->get['order'])) {
		// 		$url .= '&order=' . $this->request->get['order'];
		// 	}

		// 	if (isset($this->request->get['page'])) {
		// 		$url .= '&page=' . $this->request->get['page'];
		// 	}

		// 	$this->response->redirect($this->url->link('catalog/car', 'user_token=' . $this->session->data['user_token'] . $url, true));
		// }

        $this->getForm();

    }

    public function edit(){
        $this->load->language('catalog/car');

        $this->document->setTitle($this->language->get('heading_title'));

        // $this->load->model('catalog/car');

        // if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		// 	$this->model_catalog_product->editcar($this->request->get['car_id'], $this->request->post);

		// 	$this->session->data['success'] = $this->language->get('text_success');

		// 	$url = '';

		// 	if (isset($this->request->get['filter_name'])) {
		// 		$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		// 	}

		// 	if (isset($this->request->get['filter_model'])) {
		// 		$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		// 	}

		// 	if (isset($this->request->get['filter_price'])) {
		// 		$url .= '&filter_price=' . $this->request->get['filter_price'];
		// 	}

		// 	if (isset($this->request->get['filter_quantity'])) {
		// 		$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		// 	}

		// 	if (isset($this->request->get['filter_status'])) {
		// 		$url .= '&filter_status=' . $this->request->get['filter_status'];
		// 	}

		// 	if (isset($this->request->get['sort'])) {
		// 		$url .= '&sort=' . $this->request->get['sort'];
		// 	}

		// 	if (isset($this->request->get['order'])) {
		// 		$url .= '&order=' . $this->request->get['order'];
		// 	}

		// 	if (isset($this->request->get['page'])) {
		// 		$url .= '&page=' . $this->request->get['page'];
		// 	}

		// 	$this->response->redirect($this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'] . $url, true));
        // }
        
        $this->getForm();
    }

    public function copy(){

    }

    public function delete(){

    }

    protected function getList() {

        $data['breadcrumbs'] = array();
        $url = '';

        $data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/car', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        
        $data['add'] = $this->url->link('catalog/car/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['copy'] = $this->url->link('catalog/car/copy', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/car/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        
        $this->response->setOutput($this->load->view('catalog/car_list', $data));
    }

    protected function getform(){
        

        $url = '';

        $data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/car', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );
        
        if (!isset($this->request->get['car_id'])) {
			$data['action'] = $this->url->link('catalog/car/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/car/edit', 'user_token=' . $this->session->data['user_token'] . '&car_id=' . $this->request->get['car_id'] . $url, true);
        }
        
        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['current_year'] = date("o");



        $this->response->setOutput($this->load->view('catalog/car_form', $data));
    }
}
$(document).ready(function (e) {
    // Home Slider
    var mySwiper = new Swiper('.swiper-container.home', {
        loop: true,
        spaceBetween: 20,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });

    var swiper = new Swiper('.swiper-container.categories', {
        slidesPerView: 3,
        slidesPerColumn: 2,
        spaceBetween: 20,
        navigation: {
            nextEl: '.swiper-button-next.forCategories',
            prevEl: '.swiper-button-prev.forCategories'
        },
        breakpoints: {
            600: {
                slidesPerColumn: 3,
                slidesPerView: 1,
            },
            1240: {
                slidesPerColumn: 2,
                slidesPerView: 2,
            }
        }
    });
    $(".commonTabs").tabs();

    $('.compose').click(function () {
        $('.totalOrders').css('display', 'none');
        $('.messageContent').css('display', 'block');
    });

    //scroll button in footer
    $('.scroll').click(function () {
        $('html, body').animate({
            scrollTop: ($('#firstSection').offset().top)
        }, 1000)
    });


    // remove star from input when start writing

    $('.requiredSelect .toChange, .required .toChange').on('focus', function () {
        $(this).parent().find('.star').css('display', 'none');
        $(this).addClass('padding-rem')
    });

    $('.requiredSelect .toChange, .required .toChange').on('blur', function () {
        if ($(this).val().length == 0) {
            $(this).parent().find('.star').css('display', 'block');
            $(this).removeClass('padding-rem')
        }
    });

    $('.requiredSelect select').change(function () {
        $(this).parent().find('span').css('display', 'none');
        $(this).css({
            color: '#000',
            padding: '0 .75rem'
        });
    });

    $('.messagePopup a').click(function () {
        var parent = $(this).parents().find('.messagePopup');
        if (!(parent.hasClass('popupOpened'))) {
            parent.addClass('popupOpened')
            if (window.innerWidth >= 769) {
                var height = 85 - parent.height();
            }
            else {
                var height = 65 - parent.height();
            }
        }
        else {
            parent.removeClass('popupOpened');
            height = 0
        }


        $(parent).css('top', height)
    });

    $('.openMenu').click(function () {
        if ($('.responsiveMenu').hasClass('openResponsive')) {
            $('.openMenu').removeClass('opened');
            $('.responsiveMenu').removeClass('openResponsive');
            $('.bars').removeClass('transform')
        }

        else {
            $('.openMenu').addClass('opened');
            $('.responsiveMenu').addClass('openResponsive');
            $('.bars').addClass('transform')
        }
    });

    let screenWidth = $(window).innerWidth();
    if (screenWidth < 769) {
        // try convert to drop down
        $(".commonTabs").tabs("destroy");
        $('.forTabs > div').removeAttr('style');

        $('.commonTabs ul, .commonTabsDiff ul').each(function () {
            let subList = false;
            if ($(this).hasClass('secondList')) {
                subList = true
            }
            var list = $(this),
                select = $(document.createElement('select')).addClass('changedSelect cat').insertBefore($(this).hide()).change(function (el) {
                    let id = this.value;
                    let parent = $(this).parents('.commonTabs').find('.forTabs').eq(0);
                    if (!subList) {
                        parent.children().css('display', 'none');
                        $(parent).find('#' + id).css('display', 'block');
                    }
                });
            $('>li a', this).each(function () {

                var option = $(document.createElement('option'))
                    .appendTo(select)
                    .val(this.getAttribute('data-attr'))
                    .attr('data-index', $(this).attr('id'))
                    .addClass($(this).attr('href'))
                    .html($(this).html());
                if ($(this).attr('class') === 'selected') {
                    option.attr('selected', 'selected');
                }
                if ($(this).attr('class') === 'hide') {
                    option.attr('disabled', 'disabled');
                }
            });
            list.remove();
        });
    }

    $('.subImages a img').click(function () {
        var toReplace = $(this).parents().closest('.imgContainer').find('.imgToReplace')[0];
        $(toReplace).attr('src', $(this).attr('src'))
    });

    // $('.itemData').each(function(){ const ps = new PerfectScrollbar($(this)[0]); });
    if ($('.partDetails .itemData').length != 0) {
        const ps = new PerfectScrollbar('.partDetails .itemData');
    }


    // popups
    $('.openPopup').click(function (e) {
        $(this).parents('.popupSection').find('.commonPopup').css('display', 'flex')
        // $('.tabSection .commonPopup').css('display', 'flex')
    });

    $('.accessories .closePopup').click(function (e) {
        var mainParent = $(this).parents('.commonPopup');
        mainParent.find('.breadcrumb li:nth-child(1)').siblings().remove();
        mainParent.find('.breadcrumb').append('<li class="breadcrumb-item"><a href="#" class="Year">Year</a></li>');
        $('.accessories .allYears').css('display', 'none');
    });

    $(document).on('click','.makePopup .mainButton',function () {
        var val = $(this).text();
        var parent = $(this).parents('.commonPopup');
        parent.find('.yearsContent .breadcrumb-item').first().find('a').addClass('makeClass').text(val);
        parent.find('.makePopup').css('display', 'none');
        parent.find('.yearsContent').css('display', 'block');
        parent.find('.allYears1').css('display', 'block')
    });

    $(document).on('click', '.allYears .mainButton', function (e) {
        var mainParent = $(this).parents('.commonPopup');
        var val = $(this).text();
        var parent = $(this).parents('.allYears');

        if ( parent.length != 0 ) {
            var className = parent[0].classList[2];
            mainParent.find('.breadcrumb').children().last().find('a').addClass(className).text(val);
        }
        mainParent.find('.breadcrumb').append('<li class="breadcrumb-item"><a href="#" class="Model">Model</a></li>');
        if ( parent.hasClass('model') ) {
            mainParent.find('.breadcrumb').children().last().remove();
            mainParent.find('.breadcrumb').append('<li class="breadcrumb-item" ><a href="#" class="Edition">Edition</a></li>');
        }

        if (!(parent.hasClass('Edition'))) {
            $(this).parents('.allYears').css('display', 'none');
            $(this).parents('.allYears').next().css('display', 'block')
        }
    });

    $(document).on('click', '.breadcrumb-item a' ,function () {
        var className = $(this).attr('class');
        var parent = $(this).parents('.commonPopup');
        if (className == 'makeClass') {
            parent.find('.yearsContent').css('display', 'none');
            parent.find('.allYears').css('display', 'none');
            if ($(window).width() > 769) {
                parent.find('.makePopup').css('display', 'flex');
            }
            else {
                parent.find('.makePopup').css('display', 'block');
            }
        }
        else {
            $(this).parent().nextAll().remove();
            $(this).text(className);
            parent.find('.allYears').css('display', 'none');
            parent.find('.yearsContent').css('display', 'block');
            parent.find('.allYears.' + className).css('display', 'block')
        }
    });

    $(document).on('click', '.closePopup', function (e) {
        $(this).parents('.popupSection').find('.commonPopup').css('display', 'none')
        // $('.tabSection .commonPopup').css('display', 'none')
    });

    // popups


    $('.itemData').bind('scroll', function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            $('.opacityDiv').css('height', 0)
        }
        else {
            $('.opacityDiv').css('height', '90px')
        }
    });

    $('.cat').selectmenu();


    $('#scion > span ,#toyota > span').addClass('scionSpan');


    // $(".scionSpan").prev().on("selectmenuselect", function (event, ui) {
    //     var selectedIndex = $(event.target).find(':selected').attr('data-index');
    //     console.log(selectedIndex)
    // });


    $(".commonTabs > .cat").on("selectmenuchange", function (event, ui) {
        let subList = false;
        if ($(this).hasClass('secondList')) {
            subList = true
        }
        let id = this.value;
        let parent = $(this).parents('.commonTabs').find('.forTabs').eq(0);
        if (!subList) {
            parent.children().css('display', 'none');
            $(parent).find('#' + id).css('display', 'block');
        }
    });

    $(".orders #settings .cat, .signing .cat").on("selectmenuchange", function (event, ui) {
        var selectText = $(this).parent().find(".ui-selectmenu-button.ui-button");
        $(this).parent().find('.star').css('display', 'none');
        selectText.css({
            color: "#000",
            padding: '0 26px 0 14px'
        });
    });

    if (!($('.cat').parent().hasClass('catContaienr'))) {
        $('.ui-selectmenu-menu .ui-menu, .ui-selectmenu-menu .ui-menu').css('margin-top', '20px')
    }

    // drop down background
    $('.ui-selectmenu-button.ui-button').one('click', function () {
        if ($('.ui-selectmenu-open').height() >= 227) {
            $('.ui-selectmenu-open ').append('<div class="opacityDiv two"></div>');
        }
    });

    $('.ui-selectmenu-menu .ui-menu').bind('scroll', function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            $('.opacityDiv').css('height', 0)
        }
        else {
            $('.opacityDiv').css('height', '50px')
        }
    });

    // only two sides border
    if (($('.ui-selectmenu-button').parents('.width-32').hasClass('twoSides'))) {
        var arr = $('.width-32 .ui-selectmenu-button.ui-button');
        arr.each(function (index, el) {
            var id = $(el).attr('id');
            var list = $("body").find('ul[aria-labelledby=\"' + id + '\"]');
            list.addClass('twoSidesBorder')
        });
    }
    if (window.innerWidth < 769) {
        if (($('.ui-selectmenu-button').parents('.change').hasClass('deleteBorder'))) {
            var element = $(this).find('.ui-selectmenu-button.ui-button');
            var id = $(element).attr('id');
            var list = $("body").find(`[aria-labelledby='${id}']`);
            list.addClass('noBorder')
        }
    }

    $('.search a').click(function (e) {
        $('.formToHide').css('display', 'block')
    });

    $('body').click(function () {
        $('.formToHide').css('display', 'none')
    });

    $('.search, .formToHide input').click(function (e) {
        e.stopPropagation();
        e.preventDefault();
    });

    $('.cartPopup .first .ul').each(function (index) {
        const ps2 = window['scrollbar_'+index]= new PerfectScrollbar(this, {
            suppressScrollX: true
        });
    });

    $( ".twoSides .cat" ).on( "selectmenuselect", function( event, ui ) {
        var link = ui.item.element[0].getAttribute('class');
        window.location = link;
    } );

});
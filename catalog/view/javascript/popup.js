$(document).on('click', '.resetPopup .closePopup', function (e) {
    var mainParent = $(this).parents('.commonPopup');
    mainParent.css('display', 'none');
    mainParent.find('.breadcrumb li:nth-child(1)').siblings().remove();
    mainParent.find('.breadcrumb').append('<li class="breadcrumb-item"><a href="#">Year</a></li>');
    mainParent.find('.yearsContent').css('display', 'none');
    var allYears = mainParent.find('.allYears');
    allYears.css('display', 'none');
    allYears.find('.row').empty();
    if ($(window).width() > 769) {
        mainParent.find('.makePopup').css('display', 'flex');
    }
    else {
        mainParent.find('.makePopup').css('display', 'block');
    }
});


$(document).ready(function (e) {

    $(".scionSpan").prev().on("selectmenuselect", function (event, ui) {
        var selectedIndex = $(event.target).find(':selected').attr('data-index');
        console.log(selectedIndex)
        $("#"+selectedIndex).click();
    });

});
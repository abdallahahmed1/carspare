function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

$(document).ready(function () {
    // Highlight any found errors
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();

        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });

    // Currency
    $('#form-currency .currency-select').on('click', function (e) {
        e.preventDefault();

        $('#form-currency input[name=\'code\']').val($(this).attr('name'));

        $('#form-currency').submit();
    });

    // Language
    $('#form-language .language-select').on('click', function (e) {
        e.preventDefault();

        $('#form-language input[name=\'code\']').val($(this).attr('name'));

        $('#form-language').submit();
    });

    /* Search */
    $('#search input[name=\'search\']').parent().find('button').on('click', function () {
        var url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header #search input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search-form').submit(function (e) {
        e.preventDefault();
        search = $("#search-form input[name='search']").val();
        if (search != null) {
            url = $('base').attr('h') + 'index.php?route=product/search' + "&search=" + search;
            window.location = url;
        }

    })

    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header #search input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    // Menu
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();

        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());

        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 10) + 'px');
        }
    });

    // Product List
    $('#list-view').click(function () {
        $('#content .product-grid > .clearfix').remove();

        $('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
        $('#grid-view').removeClass('active');
        $('#list-view').addClass('active');

        localStorage.setItem('display', 'list');
    });

    // Product Grid
    $('#grid-view').click(function () {
        // What a shame bootstrap does not take into account dynamically loaded columns
        var cols = $('#column-right, #column-left').length;

        if (cols == 2) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
        } else {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
        }

        $('#list-view').removeClass('active');
        $('#grid-view').addClass('active');

        localStorage.setItem('display', 'grid');
    });

    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
        $('#list-view').addClass('active');
    } else {
        $('#grid-view').trigger('click');
        $('#grid-view').addClass('active');
    }

    // Checkout
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });

    // tooltips on hover
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});

    // Makes tooltips work on ajax generated content
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});

// Cart add remove functions
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {

                $('.alert-dismissible, .text-danger').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    }, 100);

                    //$('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart-number').load('index.php?route=common/cart/info #cart-number');
                    $.ajax({
                        url: 'index.php?route=checkout/cart/info',
                        type: 'POST',
                        success: function(data){
                            $('.cartData, .shoppingCart').replaceWith(data);
                        }
                    });
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                $('#cart-number').load('index.php?route=common/cart/info #cart-number');
                $.ajax({
                    url: 'index.php?route=checkout/cart/info',
                    type: 'POST',
                    success: function(data){
                        $('.cartData, .shoppingCart').replaceWith(data);
                    }
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (key) {
        var self=this;
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);


                $('#cart-number').load('index.php?route=common/cart/info #cart-number');

                $.ajax({
                    url: 'index.php?route=checkout/cart/info',
                    type: 'POST',
                    success: function(data){
                        $('.cartData').replaceWith(data);
                    }
                });


            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'more': function () {
        var checkedArray = [];
        var checked = $('#view-cart').find('input:checkbox:checked');
        checked.each(function (key, val) {
            checkedArray.push($(val).val())
            cart.add($(val).val());
        });
    },
    'clear':function(){
        $.ajax({
            url: 'index.php?route=checkout/cart/clear',
            type: 'post',
            success: function (json) {
                $('#cart-number').load('index.php?route=common/cart/info #cart-number');

                $.ajax({
                    url: 'index.php?route=checkout/cart/info',
                    type: 'POST',
                    success: function (data) {
                        $('.cartData').replaceWith(data);
                    }
                });


            }
        });
    }

};


// garage add remove functions
var garage = {
    'add': function (car_id, edition_id=null) {
        $.ajax({
            url: 'index.php?route=checkout/garage/add',
            type: 'post',
            data: 'car_id=' + car_id + '&edition_id=' + edition_id,
            dataType: 'json',
            success: function (json) {
                if (!json['error']) {
                    // console.log(json['garage']);
                    $('#garage').load('index.php?route=common/garage/info ul li');
                    $('#garage-number').load('index.php?route=common/garage/info #garage-number');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function (car_id) {
        $.ajax({
            url: 'index.php?route=checkout/garage/remove',
            type: 'post',
            data: 'car_id=' + car_id,
            dataType: 'json',
            success: function (json) {
                $('#garage').load('index.php?route=common/garage/info ul li');
                $('#garage-number').load('index.php?route=common/garage/info #garage-number');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'save': function (car_id) {
        $.ajax({
            url: 'index.php?route=checkout/garage/save',
            type: 'post',
            data: 'car_id=' + car_id,
            dataType: 'json',
            success: function (json) {

                console.log();
                $('#garage').load('index.php?route=common/garage/info ul li');
                $('#garage-number').load('index.php?route=common/garage/info #garage-number');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'clear': function () {
        $.ajax({
            url: 'index.php?route=checkout/garage/clear',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    // console.log(json['garage']);
                    $('#garage').load('index.php?route=common/garage/info ul li');
                    $('#garage-number').load('index.php?route=common/garage/info #garage-number');
                }
                if(window.scrollbar_1){
                    window.scrollbar_1.destroy();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var type='';
var cat_id=0;

var car_pop = {

    'index': function (ptype, cat = '') {
         type = ptype;
         cat_id = cat;

        var self = this;

             $.ajax({
                 url: 'index.php?route=product/options/manufactures',
                 type: 'post',
                 data: 'ptype=' + type +'&cat_id=' + cat_id,
                 success: function (json) {

                     var mainParent = $(self).parents('.popupSection');
                        console.log(mainParent.find('#man'));
                     mainParent.find('#man').empty();

                     for (var key in json['manufacturers']) {
                         if (json['manufacturers'].hasOwnProperty(key)) {
                             console.log(json['manufacturers'][key]['id']);
                             mainParent.find('#man').append(`
                            
                                <a href="javascript:void(0)"  onclick="car_pop.manufacturer.call(this,`+json['manufacturers'][key]['id']+`)" class="mainButton">
                                    `+json['manufacturers'][key]['name']+`
                                </a>
                            
                         `);
                         }
                     }

                 },
                 error: function (xhr, ajaxOptions, thrownError) {
                     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                 }
             });

    },
    'manufacturer': function (manu_id) {

        var self = this;
        $.ajax({
            url: 'index.php?route=product/options/years',
            type: 'post',
            data: 'manu_id=' + manu_id +'&ptype=' + type +'&cat_id=' + cat_id,
            dataType: 'json',
            success: function (json) {

                var mainParent = $(self).parents('.popupSection');

                var parent = $('.accessories .makePopup .mainButton').parents('.commonPopup');
                parent.find('.makePopup').css('display', 'none')
                parent.find('.yearsContent').css('display', 'block')
                parent.find('.allYears1').css('display','block');

                parent.find('.breadcrumb').children().eq(0).text(json['name'])

                // console.log(json['years']);
                mainParent.find('.allYears1 .row').empty();
                for (var key in json['years']) {
                    if (json['years'].hasOwnProperty(key)) {
                        mainParent.find('.allYears1 .row').append(`
                            <div>
                                <a href="javascript:void(0)" onclick="car_pop.model.call(this,`+manu_id+`,`+json['years'][key]['year']+` )" class="mainButton">
                                    `+ json['years'][key]['year'] +`
                                </a>
                            </div>
                         `);
                    }
                }
            }
        });
    },
    'model': function(manu_id,year){
        var self = this;
        $.ajax({
            url: 'index.php?route=product/options/models',
            type: 'post',
            data: 'manu_id=' + manu_id +'&year=' + year + '&ptype=' + type + '&cat_id=' + cat_id,
            dataType: 'json',
            success: function (json) {
                var mainParent = $(self).parents('.popupSection');
                mainParent.find('#model .row').empty();
                for (var key in json['model']) {

                    if (json['model'].hasOwnProperty(key)) {

                        if(json['model'][key]['edtion']){
                            var click = 'onclick="car_pop.edition.call(this,'+ manu_id +', '+ year +', '+ json['model'][key]['id'] +')"';
                        }else {
                            var click = '';
                        }

                        mainParent.find('#model .row').append(`
                            <div>
                                <a href="javascript:void(0)" `+click+` class="mainButton">
                                    `+ json['model'][key]['name'] +`
                                </a>
                            </div>
                         `);

                    }
                }

            }
        });
    },
    'edition': function(manu_id,year,model_id){
        var self = this;
        $.ajax({
            url: 'index.php?route=product/options/editions',
            type: 'post',
            data: 'manu_id=' + manu_id +'&year=' + year + '&model_id=' + model_id +'&ptype=' + type + '&cat_id=' + cat_id,
            dataType: 'json',
            success: function (json) {
                var mainParent = $(self).parents('.popupSection');

                mainParent.find('.Edition .row').empty();

                for (var key in json['editions']) {
                    if (json['editions'].hasOwnProperty(key)) {

                        var click = 'onclick="garage.add('+ model_id +','+ json['editions'][key]['edition_id'] +')"';

                        mainParent.find('.Edition .row').append(`
                            <div>
                                <a `+ click +` href="`+ json['editions'][key]['url']  +`" class="mainButton">
                                    `+ json['editions'][key]['name'] +`
                                </a>
                            </div>
                         `);

                    }
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}


//VEHICLE function
var todo='';
var vehicle = {
    'index': function(change=''){
        todo = change;
    },
    'manufacturer': function (manu_id) {
        var self = this;
        $.ajax({
            url: 'index.php?route=product/add_vehicle/years',
            type: 'post',
            data: 'manu_id=' + manu_id + '&change=' + todo,
            dataType: 'json',
            success: function (json) {
                var mainParent = $(self).parents('.popupSection');
                mainParent.find('.allYears1 .row').empty();
                for (var key in json['years']) {
                    if (json['years'].hasOwnProperty(key)) {
                        mainParent.find('.allYears1 .row').append(`
                            <div>
                                <a href="javascript:void(0)" onclick="vehicle.model.call(this,`+manu_id+`,`+json['years'][key]['year']+` )" class="mainButton">
                                    `+ json['years'][key]['year'] +`
                                </a>
                            </div>
                         `);
                    }
                }
            }
        });
    },
    'model': function(manu_id,year){
        var self = this;
        $.ajax({
            url: 'index.php?route=product/add_vehicle/models',
            type: 'post',
            data: 'manu_id=' + manu_id +'&year=' + year + '&change=' + todo,
            dataType: 'json',
            success: function (json) {
                console.log(json)
                var mainParent = $(self).parents('.popupSection');
                mainParent.find('#model .row').empty();
                for (var key in json['model']) {

                    if (json['model'].hasOwnProperty(key)) {

                        if(json['model'][key]['edtion']){
                            var click = 'onclick="vehicle.edition.call(this,'+ manu_id +', '+ year +', '+ json['model'][key]['id'] +')"';
                        }else {
                            var click = '';
                        }

                        mainParent.find('#model .row').append(`
                            <div>
                                <a href="javascript:void(0)" `+click+` class="mainButton">
                                    `+ json['model'][key]['name'] +`
                                </a>
                            </div>
                         `);

                    }
                }
            }
        });
    },
    'edition': function(manu_id,year,model_id){
        var self = this;
        $.ajax({
            url: 'index.php?route=product/add_vehicle/editions',
            type: 'post',
            data: 'manu_id=' + manu_id +'&year=' + year + '&model_id=' + model_id +'&ptype=' + type + '&change=' + todo,
            dataType: 'json',
            success: function (json) {
                console.log(json)
                var mainParent = $(self).parents('.popupSection');

                mainParent.find('.Edition .row').empty();

                for (var key in json['editions']) {
                    if (json['editions'].hasOwnProperty(key)) {

                        if(json['editions'][key]['href'] == ''){
                            json['editions'][key]['href'] = 'javascript:void(0)';
                        }

                        var click = 'onclick="garage.add('+ model_id +','+ json['editions'][key]['edition_id'] +')"';

                        mainParent.find('.Edition .row').append(`
                            <div>
                                <a href="`+ json['editions'][key]['href'] +`" `+ click +` class="mainButton closePopup">
                                    `+ json['editions'][key]['name'] +`
                                </a>
                            </div>
                         `);

                    }
                }
            }
        });
    }
}

// view add remove functions
var view = {
    'add': function (part_id) {
        $.ajax({
            url: 'index.php?route=checkout/view/add',
            type: 'post',
            data: 'part_id=' + part_id,
            dataType: 'json',
            success: function (json) {
                if (!json['error']) {
                    $('#view').load('index.php?route=common/view/info ul li');
                    $('#view-number').load('index.php?route=common/view/info #view-number');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },

    'clear': function () {
        $.ajax({
            url: 'index.php?route=checkout/view/clear',
            type: 'post',
            dataType: 'json',
            success: function (json) {
                if (json['success']) {
                    $('#view').load('index.php?route=common/view/info ul li');
                    $('#view-number').load('index.php?route=common/view/info #view-number');
                }
                if(window.scrollbar_0){
                    window.scrollbar_0.destroy();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var voucher = {
    'add': function () {

    },
    'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                // Need to set timeout otherwise it wont update the total
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);

                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}

var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert-dismissible').remove();

                if (json['redirect']) {
                    location = json['redirect'];
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);

                $('html, body').animate({scrollTop: 0}, 'slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

var compare = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert-dismissible').remove();

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#compare-total').html(json['total']);

                    $('html, body').animate({scrollTop: 0}, 'slow');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    },
    'remove': function () {

    }
}

/* Agree to Terms */
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();

    $('#modal-agree').remove();

    var element = this;

    $.ajax({
        url: $(element).attr('href'),
        type: 'get',
        dataType: 'html',
        success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div>';
            html += '  </div>';
            html += '</div>';

            $('body').append(html);

            $('#modal-agree').modal('show');
        }
    });
});

// Autocomplete */
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();

            $.extend(this, option);

            $(this).attr('autocomplete', 'off');

            // Focus
            $(this).on('focus', function () {
                this.request();
            });

            // Blur
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });

            // Keydown
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27: // escape
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });

            // Click
            this.click = function (event) {
                event.preventDefault();

                value = $(event.target).parent().attr('data-value');

                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }

            // Show
            this.show = function () {
                var pos = $(this).position();

                $(this).siblings('ul.dropdown-menu').css({
                    top: pos.top + $(this).outerHeight(),
                    left: pos.left
                });

                $(this).siblings('ul.dropdown-menu').show();
            }

            // Hide
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }

            // Request
            this.request = function () {
                clearTimeout(this.timer);

                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }

            // Response
            this.response = function (json) {
                html = '';

                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }

                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }

                    // Get all the ones with a categories
                    var category = new Array();

                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }

                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }

                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';

                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }

                if (html) {
                    this.show();
                } else {
                    this.hide();
                }

                $(this).siblings('ul.dropdown-menu').html(html);
            }

            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));

        });
    }
})(window.jQuery);

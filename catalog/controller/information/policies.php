<?php
class ControllerInformationPolicies extends Controller
{
    public function index()
    {
        unset($this->session->data['ptype']);

        $this->load->language('information/information');

        $this->load->model('catalog/information');

        $data = array();

        $policies = $this->model_catalog_information->getPolicies();

        foreach($policies as $policy){
            $data['policies'][] = array(
              'title'           => $policy['title'],
              'description'     => html_entity_decode($policy['description'],  ENT_QUOTES, 'UTF-8')
            );
        }
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['fbi']    = $this->config->get('config_fbi');

        $this->response->setOutput($this->load->view('information/policies', $data));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/14/18
 * Time: 12:08 PM
 */
class ControllerCommonCategory extends Controller {
    public function index() {

        $data = array();

        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        /*$categories_info = $this->model_catalog_category->getChildHavePart();*/

        $q = "select DISTINCT cd.* , c.image from cs_product p join cs_product_description pd on pd.product_id = p.product_id join 
              cs_product_to_category p2c on p2c.product_id = p.product_id join cs_category_description cd on p2c.category_id = cd.category_id join cs_category_filter cf on cf.category_id = cd.category_id join cs_filter f on f.filter_id = cf.filter_id join cs_filter_description fd on fd.filter_id = f.filter_id join cs_category c on c.category_id = cd.category_id where pd.ptype = 'parts' and fd.name = 'Parts'";

        $categories_info = $this->db->query($q)->rows;

        if(isset($this->session->data['car_id'])){
            foreach($categories_info as $category){
                $data['categories'][] = array(
                    'id'        => $category['category_id'],
                    'name'      => $category['name'],
                    'image'     => $this->model_tool_image->resize($category['image'], 100, 78),
                    'href'      => $this->url->link('product/product', 'category_id=' . $category['category_id'])
                );
            }

        }else{
            foreach($categories_info as $category){
                $data['categories'][] = array(
                    'id'        => $category['category_id'],
                    'name'      => $category['name'],
                    'image'     => $this->model_tool_image->resize($category['image'], 100, 78),
                );
            }
        }

        $data['pop_up'] = $this->load->controller('common/pop_up');
        return $this->load->view('common/category', $data);
    }
}
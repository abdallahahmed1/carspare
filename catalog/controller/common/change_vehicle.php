<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/28/18
 * Time: 8:58 AM
 */
class ControllerCommonChangeVehicle extends Controller {
    public function index()
    {

        $this->load->model('catalog/items');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        $car_id = isset($this->session->data['car_id']) ? $this->session->data['car_id'] : '0';
        if (isset($this->request->get['car_id'])) {
            $car_id = $this->request->get['car_id'];
        }
        $filter['car_id'] = $car_id;

        if (isset($this->request->get['category_id'])) {
            $filter['category_id'] = $this->request->get['category_id'];
        }
        if (isset($this->request->get['edition_id'])) {
            $filter['edition_id'] = $this->request->get['edition_id'];
        }

        $car = $this->model_catalog_product->getProduct($car_id);
        $categories = $this->model_catalog_items->getAccessoriesCategories($car_id);

        $svg = false;

        if (isset($car['image']) && $car['image'] != '') {
            $image = $car['image'];
        } else {
            $image = 'no_image.png';
        }
        if (pathinfo($image)['extension'] == 'SVG' or pathinfo($image)['extension'] == 'svg')
            $svg = true;

        $data['image'] = $svg ? $server . 'image/' . $image : $this->model_tool_image->resize($image, 256, 109);
        $data['year'] = $car['year'];
        $data['brand'] = $car['manufacturer'];
        $data['model'] = $car['name'];

        $data['pop_up'] = $this->load->controller('common/pop_up2');

        return $this->load->view('common/change_vehicle', $data);
    }
}
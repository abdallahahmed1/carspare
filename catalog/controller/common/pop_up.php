<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/16/18
 * Time: 11:24 AM
 */

class ControllerCommonPopUp extends Controller
{
    public function index()
    {
        $data = array();
        $this->load->model('catalog/manufacturer');

        $manunfacturers_info = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($manunfacturers_info as $manunfacturer) {
            $data['popup_manufacturers'][] = array(
                'id' => $manunfacturer['manufacturer_id'],
                'name' => $manunfacturer['name']
            );
        }


        return $this->load->view('common/pop_up', $data);

    }

}
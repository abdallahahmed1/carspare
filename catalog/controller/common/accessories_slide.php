<?php

class ControllerCommonAccessoriesSlide extends Controller
{
    public function index()
    {
        $data = array();

        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/category');
        $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

        foreach ($manufacturers as $manufacturer) {
            $data['manufacturers'][] = array(
                'name1' => strtolower($manufacturer['name']),
                'id' => strtolower($manufacturer['manufacturer_id']),
                'name' => strtoupper($manufacturer['name']),
            );
        }

        $accessories_toyota = $this->model_catalog_category->getAccessories($data['manufacturers'][1]['id']);
        $accessories_scion = $this->model_catalog_category->getAccessories($data['manufacturers'][0]['id']);

        foreach ($accessories_toyota as $accessory_toyota) {
            $data['accessory_toyota'][] = array(
                'id' => $accessory_toyota['category_id'],
                'name' => $accessory_toyota['name'],
                'href' => $this->url->link('product/accessories', '&category_id=' . $accessory_toyota['category_id']),
                'manu_id' => $data['manufacturers'][1]['id']
            );
        }

        foreach ($accessories_scion as $accessory_scion) {
            $data['accessory_scion'][] = array(
                'id' => $accessory_scion['category_id'],
                'name' => $accessory_scion['name'],
                'href' => $this->url->link('product/accessories', '&category_id=' . $accessory_scion['category_id']),
                'manu_id' => $data['manufacturers'][0]['id']
            );
        }

        $this->load->language('extension/module/featured');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $this->load->model('setting/module');

        $setting = $this->model_setting_module->getModule(28);


        $data['products'] = array();

        $setting['limit'] = 10;

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int)$setting['limit']);

            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);

                if ($product_info) {
                    if ($product_info['image']) {
                        $image1 = $this->model_tool_image->resize($product_info['image'], 392, 594);
                        $image2 = $this->model_tool_image->resize($product_info['image'], 392, 287);
                    } else {
                        $image1 = $this->model_tool_image->resize('placeholder.png', 392, 594);
                        $image2 = $this->model_tool_image->resize('placeholder.png', 392, 287);
                    }

                    $price = $product_info['price'];

                    if ((float)$product_info['special']) {
                        $special = $product_info['special'];
                    } else {
                        $special = false;
                    }

                    $data['products'][] = array(
                        'product_id'        => $product_info['product_id'],
                        'manufacturer_id'   => $product_info['manufacturer_id'],
                        'thumb_large'       => $image1,
                        'thumb_small'       => $image2,
                        'name'              => $product_info['name'],
                        'description'       => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price'             => $price,
                        'special'           => $special,
                        'href'              => $this->url->link('product/accessories/show', 'product_id=' . $product_info['product_id']),
                        'year'              => $product_info['year'],
                        'model'             => $product_info['model'],
                        'manufacturer'      => $product_info['manufacturer'],
                        'car_id'            => $product_info['car_id'],
                        'edition'           => $product_info['edition']
                    );


                }
            }
        }

        /*     var_dump($product_info);
     die();*/
        foreach ($data['products'] as $product) {
            if ($product['manufacturer_id'] == $data['manufacturers'][1]['id']) {
                $data['toyota_products'][] = $product;
            }

            if ($product['manufacturer_id'] == $data['manufacturers'][0]['id']) {
                $data['scion_products'][] = $product;
            }
        }
        $data['pop_up'] = $this->load->controller('common/pop_up');
        return $this->load->view('common/accessories_slide', $data);
    }
}
<?php
class ControllerCommonHome extends Controller {
	public function index() {
	    unset($this->session->data['ptype']);
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		$this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

		$manufacturers = $this->model_catalog_manufacturer->getManufacturers();
        $popular_products = $this->model_catalog_product->getPopularProducts(9);

        $data['popular_products'] = array();
        Foreach ($popular_products as $product){
            $popular_products[$product['product_id']]['url'] = $this->url->link('product/options/editions',['car_id' => $product['product_id'],
                                                                'brand_id' => $product['manufacturer_id'],
                                                                'year'  => $product['year'],
                                                                'ptype' => 'parts'],true);
            $popular_products[$product['product_id']]['image'] = $this->model_tool_image->resize($product['image'],296,99);

        }

        $data['popular_products'] = $popular_products;

		foreach ($manufacturers as $manufacturer){
		    $data['manufacturers'][] = array(
		        'name'  =>    $manufacturer['name'],
                'url'   =>    $this->url->link('product/options/years','&brand_id='.$manufacturer['manufacturer_id'].'&ptype=parts')
            );
        }

		$data['content_top'] = $this->load->controller('common/content_top');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['about'] = $this->load->controller('common/about_us');
		$data['category'] = $this->load->controller('common/category');
		$data['accessories_slide'] = $this->load->controller('common/accessories_slide');
		$data['select_vehicle'] = $this->load->controller('common/select_vehicle');
		$data['all_models'] = $this->url->link('product/popular','',true);

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}

<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/12/18
 * Time: 12:48 PM
 */

class ControllerCommonGarage extends Controller {
    public function index(){

        $data = array();


        if(!isset($this->session->data['garage'])){
            $data['empty'] = 'garage is empty!';
            $session = array();
        }else{
            $session = $this->session->data['garage'];

        }

        if($this->customer->isLogged()){

            $this->load->model('account/garage');
            $fake = $this->model_account_garage->getGarage($this->customer->getid());

            $data['garages'] = array_unique(array_merge($fake,$session), SORT_REGULAR);

            $data['length'] =  count($fake);

        }else{
            $data['garages'] = $session;
        }

        $data['count'] = count($data['garages']);

        $data['pop_up'] = $this->load->controller('common/pop_up2');

        return $this->load->view('common/garage', $data);
    }

    public function info() {
        $this->response->setOutput($this->index());
    }
}
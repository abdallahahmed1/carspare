<?php

class ControllerCommonAboutUS extends Controller {
    public function index()
    {
        unset($this->session->data['ptype']);

        $data = array();

        $this->load->language('information/information');

        $this->load->model('catalog/information');

        $information_info = $this->model_catalog_information->getInformation(9);
        $data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');
        $data['title'] = $information_info['title'];
        $data['meta_description'] = $information_info['meta_description'];
        /*
        var_dump($information_info);
        die();*/

        return $this->load->view('common/about_us', $data);
    }
}
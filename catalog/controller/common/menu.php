<?php

class ControllerCommonMenu extends Controller
{
    public function index()
    {
        $this->load->language('common/menu');

        // Menu
        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();
        $data['categories'][] = array(
            'name' => 'Accessories',
            'href' => $this->url->link('product/options/manufactures', 'ptype=accessories'),
            'is_active' => isset($this->session->data['ptype']) && ($this->session->data['ptype'] == 'Accessories' or $this->session->data['ptype'] == 'accessories')

        );

        $data['categories'][] = array(
            'name' => 'Parts',
            'href' => $this->url->link('product/options/manufactures', 'ptype=parts'),
            'is_active' => isset($this->session->data['ptype']) && ($this->session->data['ptype'] == 'Parts' or $this->session->data['ptype'] == 'parts')

        );

        $data['contact'] = array(
            'href' => $this->url->link('information/contact'),
            'is_active' => str_replace('amp;', '', $this->url->link('information/contact')) == "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"

        );
        $data['home'] = array(
            'href' => $this->url->link('common/home'),
            'is_active' => str_replace('amp;', '', $this->url->link('common/home')) == "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" || !isset($this->request->get['route'])

        );
        $data['login'] = array(
            'href' => $this->url->link('account/register'),
            'is_active' => str_replace('amp;', '', $this->url->link('account/register')) == "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"
        );
        return $this->load->view('common/menu', $data);
    }
}

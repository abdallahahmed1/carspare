<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/13/18
 * Time: 9:52 AM
 */

class ControllerCommonView extends Controller
{
    public function index(){
        $data = array();
        $data['count'] = 0;

        if(!isset($this->session->data['view'])){
            $data['empty'] = 'view is empty!';
        }else{
            $session = $this->session->data['view'];
            $data['views'] = $session;
            $data['count'] = count($data['views']);
        }

        return $this->load->view('common/view', $data);
    }

    public function info() {
        $this->response->setOutput($this->index());
    }
}
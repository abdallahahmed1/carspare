<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/16/18
 * Time: 11:24 AM
 */

class ControllerCommonSelectVehicle extends Controller {
    public function index()
    {
        $data = array();
        $data['pop_up'] = $this->load->controller('common/pop_up');
        return $this->load->view('common/select_vehicle', $data);
    }
}


<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 16/08/18
 * Time: 01:29 م
 */
class ControllerProductPopular extends Controller{
    public function index(){
        $data = array();
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $popular_products = $this->model_catalog_product->getPopularProducts();
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        foreach($popular_products as $product){
            $data['products'][] = array(
              'name'            => $product['name'],
              'year'            => $product['year'],
              'manufacturer'    => $product['manufacturer'],
              'image'           => $this->model_tool_image->resize($product['image'], 314, 105),
              'url'             => $this->url->link('product/options/editions',['car_id' => $product['product_id'],
                                                                            'brand_id' => $product['manufacturer_id'],
                                                                            'year'  => $product['year'],
                                                                            'ptype' => 'parts'],true)
            );
        }

        $this->response->setOutput($this->load->view('product/popular',$data));
    }
}
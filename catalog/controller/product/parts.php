<?php

class ControllerProductParts extends Controller
{

    public function index()
    {
        $this->load->model('catalog/items');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        if (!isset($this->request->get['brand_id']) && !isset($this->session->data['manufacturer_id'])) {
            $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=parts', true));
        }
        $manufacturer_id = isset($this->request->get['brand_id']) ? (int)$this->request->get['brand_id'] : $this->session->data['manufacturer_id'];
        $params = "brand_id=$manufacturer_id";
        $this->session->data['manufacturer_id'] = $manufacturer_id;

        if (!isset($this->request->get['year']) && !isset($this->session->data['car_year'])) {
            $this->response->redirect($this->url->link('product/options/years', $params, true));
        }
        $year = isset($this->request->get['year']) ? (int)$this->request->get['year'] : $this->session->data['car_year'];
        $this->session->data['car_year'] = $year;
        $params .= "&year=$year";

        if (!isset($this->request->get['car_id']) && !isset($this->session->data['car_id'])) {
            $this->response->redirect($this->url->link('product/options/models', $params, true));
        }
        $car_id = isset($this->request->get['car_id']) ? (int)$this->request->get['car_id'] : $this->session->data['car_id'];
        $this->session->data['car_id'] = $car_id;
        $params .= "&car_id=$car_id";

        $edition = true;
        if (!isset($this->request->get['edition_id']) && !isset($this->session->data['edition_id'])) {
            if ($this->model_catalog_product->getProduct($car_id)['filters']) {
                $this->response->redirect($this->url->link('product/options/editions', $params, true));
            }
            $edition = false;
        }
        $edition_id = isset($this->request->get['edition_id']) ? $this->request->get['edition_id'] : $this->session->data['edition_id'];
        $this->session->data['edition_id'] = $edition_id;
        if ($edition)
            $params .= "&edition_id=$edition_id";

        $this->urll = $this->url->link('product/parts', $params, true);


        $category_id = isset($this->request->get['category_id']) ? $this->request->get['category_id'] : null;
        $this->session->data['category_id'] = $category_id;
        if ($category_id)
            $params .= "&category_id=$category_id";

        $this->checkCompatability($manufacturer_id, $year, $car_id, $edition ? $edition_id : false);


        $car_name = $this->model_catalog_product->getProduct($car_id)['name'];
        $this->session->data['car_name'] = $car_name;
        $man_name = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id)['name'];
        $this->session->data['car_manufacturer'] = $man_name;
        $data['car_name'] = $this->model_catalog_product->getProduct($car_id)['name'];
        $data['car_year'] = $year;
        $data['car_manufacturer'] = ucfirst($man_name);
        $data['car_image'] = $this->model_tool_image->resize($this->model_catalog_product->getProduct($car_id)['image'], 258, 111);
        $data['ptype'] = "Parts";
        $data['edition_id'] = $edition_id;

        if ($edition) $data['edition_name'] = $this->getEdition($edition_id);

        $categories = $this->model_catalog_items->getPartsCategories($car_id, $edition_id);

        $data['categories'] = array();

        foreach ($categories as $key => $category){
            $categories[$key]['image'] = $this->model_tool_image->resize($category['image'], 270, 203);
            $categories[$key]['href'] = $this->url->link('product/parts/parts/',$params.'&category_id='.$category['category_id'],true);
        }
        $data['categories'] = $categories;

        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');
        $data['change_vehicle'] = $this->load->controller('common/change_vehicle');

        $this->response->setOutput($this->load->view('product/category', $data));

    }

    public function parts(){

        $this->load->model('catalog/items');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        if (!isset($this->request->get['brand_id']) && !isset($this->session->data['manufacturer_id'])) {
            $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=parts', true));
        }
        $manufacturer_id = isset($this->request->get['brand_id']) ? (int)$this->request->get['brand_id'] : $this->session->data['manufacturer_id'];
        $params = "brand_id=$manufacturer_id";
        $this->session->data['manufacturer_id'] = $manufacturer_id;

        if (!isset($this->request->get['year']) && !isset($this->session->data['car_year'])) {
            $this->response->redirect($this->url->link('product/options/years', $params, true));
        }
        $year = isset($this->request->get['year']) ? (int)$this->request->get['year'] : $this->session->data['car_year'];
        $this->session->data['car_year'] = $year;
        $params .= "&year=$year";

        if (!isset($this->request->get['car_id']) && !isset($this->session->data['car_id'])) {
            $this->response->redirect($this->url->link('product/options/models', $params, true));
        }
        $car_id = isset($this->request->get['car_id']) ? (int)$this->request->get['car_id'] : $this->session->data['car_id'];
        $this->session->data['car_id'] = $car_id;
        $params .= "&car_id=$car_id";

        $edition = true;
        if (!isset($this->request->get['edition_id']) && !isset($this->session->data['edition_id'])) {
            if ($this->model_catalog_product->getProduct($car_id)['filters']) {
                $this->response->redirect($this->url->link('product/options/editions', $params, true));
            }
            $edition = false;
        }
        $edition_id = isset($this->request->get['edition_id']) ? $this->request->get['edition_id'] : $this->session->data['edition_id'];
        $this->session->data['edition_id'] = $edition_id;
        if ($edition)
            $params .= "&edition_id=$edition_id";

        $this->urll = $this->url->link('product/parts', $params, true);

        if(!isset($this->request->get['category_id']) && !isset($this->session->data['category_id'])){
            $this->response->redirect($this->url->link('product/parts', $params, true));
        }
        $category_id = isset($this->request->get['category_id']) ? $this->request->get['category_id'] : $this->session->data['category_id'];
        $this->session->data['category_id'] = $category_id;
        $params .= "&category_id=$category_id";

        $car_name = $this->model_catalog_product->getProduct($car_id)['name'];
        $this->session->data['car_name'] = $car_name;
        $man_name = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id)['name'];
        $this->session->data['car_manufacturer'] = $man_name;
        $data['car_name'] = $this->model_catalog_product->getProduct($car_id)['name'];
        $data['car_year'] = $year;
        $data['car_manufacturer'] = ucfirst($man_name);
        $data['car_image'] = $this->model_tool_image->resize($this->model_catalog_product->getProduct($car_id)['image'], 258, 111);
        $data['ptype'] = "Parts";
        $data['edition_id'] = $edition_id;

        $top_parts = $this->model_catalog_items->getTopParts($car_id, $edition_id, $category_id);
        foreach ($top_parts as $key => $top_part){
            //$top_parts[$key]['image'] =  $this->model_tool_image->resize($top_part['image'], 200, 200);
            $top_parts[$key]['href'] = $this->url->link('product/parts/top', 'part_id='.$top_part['product_id'],true);
        }
        $data['top_parts'] = $top_parts;

        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('product/top_parts', $data));
    }

    public function top(){

        $this->load->model('catalog/items');
        $this->load->model('tool/image');

        $part = $this->model_catalog_items->getTopPart($this->request->get['part_id']);
        $car = $this->model_catalog_product->getProduct($part['part']['car_id']);
        $illustrators = $part['illustrators'];
        $children = $part['children'];
        $part = $part['part'];
        $data['car_id'] = $this->session->data['car_id'] = $part['car_id'];
        $data['car_year'] = $this->session->data['car_year'] = $part['year'];
        $data['car_name'] = $this->session->data['car_name'] = $car['name'];
        $data['top_name'] = $part['name'];
        $data['category'] = $part['sub_category'];
        $this->session->data['manufacturer_id'] = $part['manufacturer_id'];
        $data['car_manufacturer'] = $this->session->data['car_manufacturer'] = $part['manufacturer'];
        $this->session->data['edition_id'] = $part['edition_id'];
        $data['edition'] = $this->session->data['car_edition'] = $this->getEdition($part['edition_id']);
        $this->session->data['car_image'] = $this->model_catalog_product->getProduct($part['car_id'])['image'];
        $data['car_image'] =  $this->model_tool_image->resize($car['image'], 258, 111);
        $this->session->data['ptype'] = 'Parts';

        $top_parts = $data['top_parts'] = $this->model_catalog_items->getTopParts($data['car_id'], $part['edition_id'], $this->model_catalog_product->getCategories($part['product_id'])[0]['category_id']);
        foreach ($top_parts as $key => $top_part){
            $top_parts[$key]['href'] = $this->url->link('product/parts/top', 'part_id='.$top_part['product_id'],true);
            $top_parts[$key]['is_active'] = $top_part['product_id'] == $part['product_id'] ? true : false;
        }
        $data['top_parts'] = $top_parts;

        $url = $this->url->link('product/parts/parts/',"brand_id=".$part['manufacturer_id']."&year=".$part['year']."&car_id=".$part['car_id']."&edition_id=".$part['edition_id']);
        $others = $this->model_catalog_items->getPartsCategories($part['car_id'], $part['edition_id']);
        foreach ($others as $key => $category){
            $others[$key]['href'] = $url.'&category_id='.$category['category_id'];

        }
        $data['others'] = $others;

        $data['image'] = $this->model_tool_image->resize($part['image'], 275, 207);
        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }
        if (pathinfo($part['image'])['extension'] == 'SVG' or pathinfo($part['image'])['extension'] == 'svg')
            $data['image'] = $server . 'image/' . $part['image'];


        foreach ($children as $key => $child){
            $children[$key]['href'] = $this->url->link('product/parts/show/','part_id='.$child['product_id'], true);
            if($child['image']){
                $children[$key]['image'] = $this->model_tool_image->resize($child['image'], 270, 203);
                if (pathinfo($child['image'])['extension'] == 'SVG' or pathinfo($child['image'])['extension'] == 'svg')
                    $children[$key]['image'] = $server . 'image/'.$child['image'];
            }
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $children[$key]['price'] = $this->currency->format($this->tax->calculate($children[$key]['price'], $children[$key]['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $children[$key]['price'] = false;
            }

            if ((float)$children[$key]['special']) {
                $children[$key]['special'] = $this->currency->format($this->tax->calculate($children[$key]['special'], $children[$key]['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $children[$key]['special'] = false;
            }
        }
        $data['children'] = $children;
        foreach ($illustrators as $key => $illustrator){
            $illustrators[$key]['href'] = $this->url->link('product/parts/top/','part_id='.$illustrator['product_id'], true);
            if($illustrator['image']){
                $illustrators[$key]['image'] = $this->model_tool_image->resize($illustrator['image'], 82, 62);
                if (pathinfo($illustrator['image'])['extension'] == 'SVG' or pathinfo($illustrator['image'])['extension'] == 'svg')
                    $illustrators[$key]['image'] = $server . 'image/' . $illustrator['image'];

            }

        }
        $data['illustrators'] = $illustrators;

        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');
        $data['change_vehicle'] = $this->load->controller('common/change_vehicle');
        $data['sidebar'] = $this->load->view('product/parts_sidebar', $data);

        $this->response->setOutput($this->load->view('product/part', $data));

    }

    public function show(){
        $this->load->model('catalog/items');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $part = $this->model_catalog_product->getProduct($this->request->get['part_id']);
        $car = $this->model_catalog_product->getProduct($part['car_id']);
        $data['car_id'] = $this->session->data['car_id'] = $car_id = $part['car_id'];
        $data['car_name'] = $this->session->data['car_name'] = $car['name'];
        $data['car_year'] = $this->session->data['car_year'] = $year = $part['year'];
        $data['manufacturer_id'] = $this->session->data['manufacturer_id'] = $manufacturer_id = $part['manufacturer_id'];
        $data['car_manufacturer'] = $this->session->data['car_manufacturer'] = $manufacturer = $part['manufacturer'];
        $data['category'] = $part['sub_category'];
        $this->session->data['car_image'] = $image = $car['image'];
        $data['car_image'] = $this->model_tool_image->resize($image, 275, 207);
        $q = $this->db->query("Select * from ".DB_PREFIX."product_edition where product_id = ".$part['product_id'])->rows;
        if(count($q ) > 0)
            $data['edition_id'] = $this->session->data['edition_id'] = $edition_id = $q[0]['filter_id'];


        /*$top_parts = $data['top_parts'] = $this->model_catalog_items->getTopParts($data['car_id'], $edition_id, $this->model_catalog_product->getCategories($part['product_id'])[0]['category_id']);
        foreach ($top_parts as $key => $top_part){
            $top_parts[$key]['href'] = $this->url->link('product/parts/top', 'part_id='.$top_part['product_id'],true);
            $top_parts[$key]['is_active'] = $top_part['product_id'] == $part['product_id'] ? true : false;
        }
        $data['top_parts'] = $top_parts;*/

        $url = $this->url->link('product/parts/parts/',"brand_id=".$part['manufacturer_id']."&year=".$part['year']."&car_id=".$part['car_id']."&edition_id=".$edition_id);
        $others = $this->model_catalog_items->getPartsCategories($car_id, $edition_id);
        foreach ($others as $key => $category){
            $others[$key]['href'] = $url.'&category_id='.$category['category_id'];

        }
        $data['others'] = $others;

        $data['sidebar'] = $this->load->view('product/parts_sidebar', $data);


        $data['image'] = $this->model_tool_image->resize($part['image'], 270, 204);
        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }
        if (pathinfo($part['image'])['extension'] == 'SVG' or pathinfo($part['image'])['extension'] == 'svg')
            $data['image'] = $server . 'image/' . $part['image'];
        $data['name'] = $part['name'];
        $parent = $this->model_catalog_product->getProduct($this->model_catalog_items->getPartParent($part['product_id']));
        $data['core'] = $parent['price'];
        $data['special'] = $part['special'];
        $data['price'] = $part['price'];

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $data['price'] = $this->currency->format($this->tax->calculate($data['price'], $part['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
            $data['price'] = false;
        }

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $data['core'] = $this->currency->format($this->tax->calculate($data['core'], $parent['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
            $data['core'] = false;
        }

        if ((float)$data['special']) {
            $data['special'] = $this->currency->format($this->tax->calculate($data['special'], $part['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            $data['save'] = round((1-($part['special']*1.0/$part['price']))*100,PHP_ROUND_HALF_UP);
        } else {
            $data['special'] = false;
        }



        $data['part_id'] = $part['product_id'];
        $data['core_price'] = $this->model_catalog_product->getProduct($this->model_catalog_items->getPartParent($part['product_id']))['price'];
        $data['description'] = htmlspecialchars_decode($part['description']);
        $data['mpn'] = $part['mpn'];
        $data['filters'] = $part['filters'];

        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');


        $this->response->setOutput($this->load->view('product/part_details', $data));


    }

    public function getEdition($edition_id)
    {
        $q = "select  fd.name from " . DB_PREFIX . "filter_description fd where fd.filter_id = $edition_id ";
        $edition = $this->db->query($q)->rows;
        if ($edition)
            return $edition[0]['name'];
        else return null;
    }

    public function checkCompatability($brand_id = null, $year = null, $car_id = null, $edition_id = false)
    {
        if (!$brand_id || !$year || !$car_id) {
            $this->response->redirect($this->url->link('not_found'));
        }

        $q = $this->db->query("Select * from " . DB_PREFIX . "product p join " . DB_PREFIX . "product_description pd on 
                                p.product_id = pd.product_id where pd.car_id = " . (int)$car_id . " 
                                and pd.top = 1 and pd.year = " . (int)$year . " and p.manufacturer_id = " . (int)$brand_id . " ")->rows;
        if (count($q) == 0) {
            unset($this->session->data['manufacturer_id']);
            unset($this->session->data['car_year']);
            unset($this->session->data['car_id']);
            unset($this->session->data['edition_id']);

            $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=parts', true));
        }

        if ($edition_id && $edition_id != 0) {
            $q = $this->db->query("select distinct fd.* from " . DB_PREFIX . "product_description pd join " . DB_PREFIX . "product_edition pe
                                    on pd.product_id = pe.product_id join " . DB_PREFIX . "filter_description fd on fd.filter_id = pe.filter_id
                                     where pd.car_id = $car_id and pd.ptype = 'parts' and pd.top = 1 and pe.filter_id = " . (int)$edition_id)->rows;

            if (count($q) == 0) {
                unset($this->session->data['manufacturer_id']);
                unset($this->session->data['car_year']);
                unset($this->session->data['car_id']);
                unset($this->session->data['edition_id']);
                $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=parts', true));
            }
        }

    }
}
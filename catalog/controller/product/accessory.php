<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 14/08/18
 * Time: 06:04 م
 */

class ControllerProductAccessory extends Controller{

    public function index(){

        $this->load->language('product/product');
        $this->load->model('catalog/product');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['id'] = $this->request->get['id'];
        $product_info = $this->model_catalog_product->getProduct($data['id']);
        $data['product'] = $product_info;
        $data['image'] =  $this->model_tool_image->resize($product_info['image'],270,204);
        $this->response->setOutput($this->load->view('product/accessory',$data));

    }

}
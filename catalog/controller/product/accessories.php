<?php

class ControllerProductAccessories extends Controller
{
    public $urll = "";

    public function index()
    {

        $this->load->model('catalog/items');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');

        if (!isset($this->request->get['brand_id']) && !isset($this->session->data['manufacturer_id'])) {
            $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=accessories', true));
        }
        $manufacturer_id = isset($this->request->get['brand_id']) ? (int)$this->request->get['brand_id'] : $this->session->data['manufacturer_id'];
        $params = "brand_id=$manufacturer_id";
        $this->session->data['manufacturer_id'] = $manufacturer_id;

        if (!isset($this->request->get['year']) && !isset($this->session->data['car_year'])) {
            $this->response->redirect($this->url->link('product/options/years', $params, true));
        }
        $year = isset($this->request->get['year']) ? (int)$this->request->get['year'] : $this->session->data['car_year'];
        $this->session->data['car_year'] = $year;
        $params .= "&year=$year";

        if (!isset($this->request->get['car_id']) && !isset($this->session->data['car_id'])) {
            $this->response->redirect($this->url->link('product/options/models', $params, true));
        }
        $car_id = isset($this->request->get['car_id']) ? (int)$this->request->get['car_id'] : $this->session->data['car_id'];
        $this->session->data['car_id'] = $car_id;
        $params .= "&car_id=$car_id";

        $edition = true;
        if (!isset($this->request->get['edition_id']) && !isset($this->session->data['edition_id'])) {
            if ($this->model_catalog_product->getProduct($car_id)['filters']) {
                $this->response->redirect($this->url->link('product/options/editions', $params, true));
            }
            $edition = false;
        }
        $edition_id = isset($this->request->get['edition_id']) ? $this->request->get['edition_id'] : $this->session->data['edition_id'];
        $this->session->data['edition_id'] = $edition_id;
        if ($edition)
            $params .= "&edition_id=$edition_id";

        $this->urll = $this->url->link('product/accessories', $params, true);


        $category_id = isset($this->request->get['category_id']) ? $this->request->get['category_id'] : null;
        $this->session->data['category_id'] = $category_id;
        if ($category_id)
            $params .= "&category_id=$category_id";

        $this->checkCompatability($manufacturer_id, $year, $car_id, $edition ? $edition_id : false);


        $car_name = $this->model_catalog_product->getProduct($car_id)['name'];
        $this->session->data['car_name'] = $car_name;
        $man_name = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id)['name'];
        $this->session->data['car_manufacturer'] = $man_name;

        $data['car_name'] = $this->model_catalog_product->getProduct($car_id)['name'];
        $data['car_year'] = $year;
        $data['car_manufacturer'] = ucfirst($man_name);
        $data['car_image'] = $this->model_tool_image->resize($this->model_catalog_product->getProduct($car_id)['image'], 258, 111);
        $data['ptype'] = "Accessories";
        $data['edition_id'] = $edition_id;

        if ($edition) $data['edition_name'] = $this->getEdition($edition_id);

        $filter = array();

        $data['sort'] = false;
        if (isset($this->request->get['price'])) {

            $data['sort'] = true;
            if ($this->request->get['price'] == 'high') {
                $filter['price'] = "desc";
                $data['sort_high'] = true;
            } elseif ($this->request->get['price'] == 'low') {
                $filter['price'] = "asc";
                $data['sort_low'] = true;
            }
        }

        $filter['car_id'] = $car_id;
        if ($edition_id != null) {
            $filter['edition_id'] = $edition_id;
        }

        if (isset($this->request->get['category_id'])) {
            $filter['category_id'] = $this->request->get['category_id'];
        }


        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        $accessories = $this->model_catalog_items->getAccessories($filter);


        $data['accessories'] = array();

        foreach ($accessories as $accessory) {
            $svg = false;
            if (isset($accessory['image']) && $accessory['image'] != '') {
                $image = $accessory['image'];
            } else {
                $image = 'no_image.png';
            }
            if (pathinfo($image)['extension'] == 'SVG' or pathinfo($image)['extension'] == 'svg')
                $svg = true;

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($accessory['price'], $accessory['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }

            if ((float)$accessory['special']) {
                $special = $this->currency->format($this->tax->calculate($accessory['special'], $accessory['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }
            $data['accessories'][] = array(
                'name' => $accessory['name'],
                'id' => $accessory['product_id'],
                'part_number' => $accessory['mpn'],
                'brand' => $this->model_catalog_manufacturer->getManufacturer($accessory['manufacturer_id'])['name'],
                'price' => $price,
                'special' => $special,
                'image' => $svg ? $server . 'image/' . $image : $this->model_tool_image->resize($image, 270, 203),
                'href' => $this->url->link('product/accessories/show', 'product_id=' . $accessory['product_id'], true)
            );
        }

        $this->session->data['ptype'] = "accessories";
        $data['view'] = $this->sidebar();
        $data['footer'] = $this->load->controller('common/footer');
        $this->document->setTitle($data['car_manufacturer'] . " $car_name Accessories");
        $data['header'] = $this->load->controller('common/header');
        $data['sort_url'] = html_entity_decode($this->url->link('product/accessories',$params."&price=",true));
        $this->response->setOutput($this->load->view('product/accessories', $data));

    }

    public function show()
    {

        $this->load->language('product/product');
        $this->load->model('catalog/product');
        $data['sidebar'] = $this->sidebar();

        $data['product_id'] = $this->request->get['product_id'];
        $product_info = $this->model_catalog_product->getProduct($data['product_id']);
        $data['product'] = $product_info;
        $data['product']['description'] = htmlspecialchars_decode($product_info['description']);
        $this->session->data['ptype'] = "accessories";

        if ($this->request->server['HTTPS']) {
            $server = HTTPS_CATALOG;
        } else {
            $server = HTTP_CATALOG;
        }

        if ($data['product']['image']) {
            if (pathinfo($data['product']['image'])['extension'] == 'SVG' or pathinfo($data['product']['image'])['extension'] == 'svg')
                $data['product']['image'] = $server.'/image/'.$data['product']['image'];
            else
                $data['product']['image'] = $this->model_tool_image->resize($data['product']['image'], 270, 204);

        } else {
            $data['product']['image'] = $this->model_tool_image->resize('placeholder.png', 270, 204);
        }
        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $data['product']['price'] = $this->currency->format($this->tax->calculate($data['product']['price'], $data['product']['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
            $data['product']['price'] = false;
        }

        if ((float)$data['product']['special']) {
            $data['product']['special'] = $this->currency->format($this->tax->calculate($data['product']['special'], $data['product']['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
            $data['product']['special'] = false;
        }
        $data['footer'] = $this->load->controller('common/footer');
        $this->document->setTitle($data['product']['name']." / ".
            $this->session->data['car_manufacturer']." ".
            $this->session->data['car_name']." ".
            $this->session->data['car_year']);
        $data['header'] = $this->load->controller('common/header');

        $data['change_vehicle'] = $this->load->controller('common/change_vehicle');

        $this->response->setOutput($this->load->view('product/accessory', $data));

    }

    public function getEdition($edition_id)
    {
        $q = "select  fd.name from " . DB_PREFIX . "filter_description fd where fd.filter_id = $edition_id ";
        $edition = $this->db->query($q)->rows;
        if ($edition)
            return $edition[0]['name'];
        else return null;
    }

    public function sidebar()
    {
        $this->load->model('catalog/items');

        $car_id = isset($this->session->data['car_id']) ? $this->session->data['car_id'] : '0';
        if (isset($this->request->get['car_id'])) {
            $car_id = $this->request->get['car_id'];
        }

        $categories = $this->model_catalog_items->getAccessoriesCategories($car_id);

        $filter['car_id'] = $car_id;
        if (isset($this->request->get['category_id'])) {
            $filter['category_id'] = $this->request->get['category_id'];
        }
        if (isset($this->request->get['edition_id'])) {
            $filter['edition_id'] = $this->request->get['edition_id'];
        }

        $data['categories'] = array();

        foreach ($categories as $category) {
            $data['categories'][] = array(
                'name' => $category['name'],
                'id' => $category['category_id'],
                'href' => $this->urll . '&category_id=' . $category['category_id'],
                'is_active' => isset($filter['category_id']) && $filter['category_id'] == $category['category_id'] ? true : false
            );
        }
        $data['change_vehicle'] = $this->load->controller('common/change_vehicle');

        $view = $this->load->view('product/accessories_sidebar', $data);

        return $view;
    }

    public function checkCompatability($brand_id = null, $year = null, $car_id = null, $edition_id = false)
    {
        if (!$brand_id || !$year || !$car_id) {
            $this->response->redirect($this->url->link('not_found'));
        }

        $q = $this->db->query("Select * from " . DB_PREFIX . "product p join " . DB_PREFIX . "product_description pd on p.product_id = pd.product_id where pd.product_id = " . (int)$car_id . " 
              and pd.year = " . (int)$year . " and p.manufacturer_id = " . (int)$brand_id . " ")->rows;

        if (count($q) == 0) {
            unset($this->session->data['manufacturer_id']);
            unset($this->session->data['car_year']);
            unset($this->session->data['car_id']);
            unset($this->session->data['edition_id']);

            $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=accessories', true));
        }

        if ($edition_id && $edition_id != 0) {
            $q = $this->db->query("select distinct fd.* from " . DB_PREFIX . "product_description pd join " . DB_PREFIX . "product_edition pe on pd.product_id = pe.product_id join 
                " . DB_PREFIX . "filter_description fd on fd.filter_id = pe.filter_id where pd.car_id = $car_id and pd.ptype = 'accessories' and pe.filter_id = " . (int)$edition_id)->rows;
            if (count($q) == 0) {
                unset($this->session->data['manufacturer_id']);
                unset($this->session->data['car_year']);
                unset($this->session->data['car_id']);
                unset($this->session->data['edition_id']);
                $this->response->redirect($this->url->link('product/options/manufactures', 'ptype=accessories', true));
            }
        }

    }
}

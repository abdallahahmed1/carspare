<?php
class ControllerProductCategory extends Controller {
	public function index() {
        $this->load->model('catalog/items');

        $filter = array();
        $car_id = isset($this->session->data['car_id']) ? $this->session->data['car_id'] : '0';
        if(isset($this->request->get['car_id']))
            $car_id = $this->request->get['car_id'];

        $filter['car_id'] = $car_id;

        if(isset($this->request->get['price'])){
            if($this->request->get['price'] == 'high')
                $filter['price'] = "desc";
            elseif ($filter['price'])
                $filter['price'] = "asc";
        }

        $this->model_catalog_items->getAccessories($filter);
	}
}

<?php

/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/27/18
 * Time: 7:30 AM
 */
class ControllerProductAddVehicle extends Controller
{
    public function years()
    {
        $json = array();
        $manufacturer_id = $this->request->post['manu_id'];
        $change = $this->request->post['change'];

        if (!empty($change)) {
            $ptype = $this->session->data['ptype'];

            $query = $this->db->query('Select distinct pd.year from ' . DB_PREFIX . 'product p JOIN ' . DB_PREFIX . 'product_description pd on p.product_id = pd.product_id join '. DB_PREFIX .'product_to_category pc on p.product_id = pc.product_id where 
                (p.manufacturer_id = "' . $manufacturer_id . '" and p.product_id = pd.product_id  AND pd.ptype = "' . $this->db->escape($ptype) . '") ORDER BY pd.year ASC
            ');

        } else {
            $query = $this->db->query('Select distinct pd.year from ' . DB_PREFIX . 'product p JOIN ' . DB_PREFIX . 'product_description pd where 
               ( p.manufacturer_id = "' . $manufacturer_id . '" and p.product_id = pd.product_id ) ORDER BY pd.year ASC
            ');
        }

        foreach ($query->rows as $result) {
            $data ['years'][] = array(
                'year' => $result['year'],
            );
        }

        $json['success'] = 'done';
        $json['years'] = $data['years'];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function models()
    {
        $json = array();
        $manufacturer_id = $this->request->post['manu_id'];
        $year = $this->request->post['year'];
        $change = $this->request->post['change'];

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        if(!empty($change)){
            $ptype = $this->session->data['ptype'];
//
            $models = $this->db->query('Select distinct pd2.* , p.image from '. DB_PREFIX .'product_description pd join '. DB_PREFIX .'product_to_category pc on pd.product_id = pc.product_id 
                                join '. DB_PREFIX .'product_description pd2 on pd2.product_id = pd.car_id join '.DB_PREFIX.'product p on p.product_id = pd2.product_id where pd.ptype="'. $ptype .'" AND pd2.year ='.$year)->rows;

        }else{
            $models = $this->db->query('Select distinct pd2.* , p.image from '. DB_PREFIX .'product_description pd join '. DB_PREFIX .'product_to_category pc on pd.product_id = pc.product_id
                                join '. DB_PREFIX .'product_description pd2 on pd2.product_id = pd.car_id join '.DB_PREFIX.'product p on p.product_id = pd2.product_id where pd.ptype="parts" AND pd2.year ='.$year)->rows;
        }


        foreach ($models as $model) {
            $edtion = $this->model_catalog_product->getProduct($model['product_id'])['filters'] > 0;
            $data ['models'][] = array(
                'id' => $model['product_id'],
                'name' => $model['name'],
                'year' => $model['year'],
                'edtion' => $edtion,
                'image' => $this->model_tool_image->resize($model['image'], 314, 105)
            );

        }

        $json['success'] = 'done';
        $json['model'] = $data['models'];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function editions()
    {
        $this->load->model('catalog/product');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');

        $car_id = $this->request->post['model_id'];
        $year = $this->request->post['year'];
        $manufacturer_id = $this->request->post['manu_id'];
        $change = $this->request->post['change'];

        if (!empty($change)) {
            $ptype = $this->session->data['ptype'];
            $q = "select distinct fd.* from cs_product_description pd join cs_product_edition pe on pd.product_id = pe.product_id join 
                cs_filter_description fd on fd.filter_id = pe.filter_id join cs_product_to_category pc on pd.product_id = pc.product_id where pd.car_id = $car_id and pd.ptype = '$ptype' ";
        }else{
            $q = " select distinct fd.* from cs_product_description pd join cs_product_edition pe on pd.product_id = pe.product_id join 
                cs_filter_description fd on fd.filter_id = pe.filter_id where pd.car_id = $car_id ";
        }

        $editions = $this->db->query($q)->rows;
        $data['editions'] = array();

        foreach ($editions as $edition) {
            if (!empty($change)) {
                if($ptype == 'Parts'){
                    $href = $this->url->link('product/' . 'parts' , "brand_id=$manufacturer_id&year=$year&car_id=$car_id&edition_id=" . $edition['filter_id'], true);
                }else{
                    $href = $this->url->link('product/' . $ptype, "brand_id=$manufacturer_id&year=$year&car_id=$car_id&edition_id=" . $edition['filter_id'], true);
                }

            } else {
                $href = '';
            }

            $data['editions'][] = array(
                'name' => $edition['name'],
                'edition_id' => $edition['filter_id'],
                'href' => $href,
            );
        }

        $json['success'] = 'done';
        $json['editions'] = $data['editions'];

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
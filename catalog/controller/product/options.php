<?php

class ControllerProductOptions extends Controller
{

    public function index()
    {
        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');
        $data['ptype'] = array();

        $data['ptype'][] = array(
            'name' => 'Accessories',
            'href' => $this->url->link('product/options/manufactures', 'ptype=accessories')
        );
        $data['ptype'][] = array(
            'name' => 'Parts',
            'href' => $this->url->link('product/options/manufactures', 'ptype=parts')
        );
        $this->response->setOutput($this->load->view('product/items_list', $data));
    }

    public function manufactures()
    {
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $json = array();
            $ptype = $this->request->post['ptype'];
            $cat_id = $this->request->post['cat_id'];

            if(!empty($cat_id)){
                $q = $this->db->query("SELECT DISTINCT m.manufacturer_id , m.name FROM `cs_product_description` pd JOIN cs_product p on pd.product_id = p.product_id JOIN cs_product_to_category pc on p.product_id = pc.product_id JOIN cs_manufacturer m on m.manufacturer_id = p.manufacturer_id WHERE pd.ptype = '$ptype' AND pc.category_id = $cat_id")->rows;
            }else{
                $q = $this->db->query("SELECT DISTINCT m.manufacturer_id , m.name FROM `cs_product_description` pd JOIN cs_product p on pd.product_id = p.product_id JOIN cs_product_to_category pc on p.product_id = pc.product_id JOIN cs_manufacturer m on m.manufacturer_id = p.manufacturer_id WHERE pd.ptype = '$ptype'")->rows;
            }


            foreach ($q as $manu){

                $data['manufacturers'][] = array(
                    'name'  => $manu['name'],
                    'id'    => $manu['manufacturer_id']
                );

            }

            $json['success'] = 'done';
            $json['manufacturers'] = $data['manufacturers'];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

        }else{
            if (isset($this->session->data['car_id']) && (lcfirst($this->request->get['ptype']) == 'accessories' || lcfirst($this->request->get['ptype']) == 'parts')) {

                $this->session->data['ptype'] = ucfirst($this->request->get['ptype']);
                $url = $this->url->link(
                    'product/' . lcfirst($this->request->get['ptype']), true);

                if (isset($this->session->data['manufacturer_id'])) {
                    $url .= '&brand_id=' . (int)$this->session->data['manufacturer_id'];
                } else {
                    unset($this->session->data['manufacturer_id']);
                    unset($this->session->data['car_year']);
                    unset($this->session->data['car_id']);
                    unset($this->session->data['edition_id']);
                    $data['header'] = $this->load->controller('common/header');
                    $this->response->redirect($url);
                }
                $edition = $this->session->data['edition_id'] ? '&edition_id=' . $this->session->data['edition_id'] : '';
                $this->response->redirect($this->url->link(
                    'product/' . lcfirst($this->request->get['ptype']),
                    '&brand_id=' . (int)$this->session->data['manufacturer_id'] .
                    '&year=' . (int)$this->session->data['car_year'] .
                    '&car_id=' . (int)$this->session->data['car_id'] .
                    $edition
                    , true));
            } else {

                $data['ptype'] = isset($this->request->get['ptype']) ? ucfirst($this->request->get['ptype']) : null;
                if (!$data['ptype'] && !isset($this->session->data['ptype']))
                    $this->response->redirect($this->url->link('product/options', '', true));

                $manufacturers = $this->db->query("SELECT manufacturer_id, name from " . DB_PREFIX . "manufacturer  ")->rows;
                $this->session->data['ptype'] = lcfirst($data['ptype']);
                $data['header'] = $this->load->controller('common/header');
                $data['footer'] = $this->load->controller('common/footer');

                if (isset($this->request->get['brand_id'])) {
                    $brand_id = $this->request->get['brand_id'];
                    $this->session->data['manufacturer_id'] = $brand_id;
                    $this->response->redirect($this->url->link('product/options/years', 'brand_id=' . $brand_id, true));
                } else {

                    $data['manufacturers'] = array();
                    foreach ($manufacturers as $manufacturer) {
                        $data['manufacturers'][] = array(
                            'name' => $manufacturer['name'],
                            'id' => $manufacturer['manufacturer_id'],
                            'href' => $this->url->link('product/options/years', 'brand_id=' . $manufacturer['manufacturer_id'])
                        );
                    }
                    $this->response->setOutput($this->load->view('product/manufacturer_list', $data));
                }

            }
        }

    }

    public function years()
    {
        $cat_id = '';
        $this->load->model('catalog/manufacturer');
        $data = array();
        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $json = array();
            $manufacturer_id = $this->request->post['manu_id'];
            $ptype = $this->request->post['ptype'];
            $cat_id = $this->request->post['cat_id'];
        } else {

            if (!isset($this->request->get['ptype']) && !isset($this->session->data['ptype'])) {

                $this->response->redirect($this->url->link('product/options', '', true));
            }

            if (!isset($this->request->get['brand_id']) && !isset($this->session->data['manufacturer_id'])) {
                $params = isset($this->request->get['ptype']) ? $this->request->get['ptype'] : $this->session->data['ptype'];

                $this->response->redirect($this->url->link('product/options/manufactures', "ptype=$params", true));
            }

            $manufacturer_id = (int)$this->request->get['brand_id'];
            $ptype = isset($this->request->get['ptype']) ? $this->request->get['ptype'] : lcfirst($this->session->data['ptype']);
            $this->session->data['manufacturer_id'] = $manufacturer_id;
            $data['car_manufacturer'] = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id)['name'];
            $this->session->data['car_manufacturer'] = $data['car_manufacturer'];
        }
        if (!empty($cat_id)) {
            $query = $this->db->query('Select distinct pd.year from ' . DB_PREFIX . 'product p JOIN ' . DB_PREFIX . 'product_description pd on p.product_id = pd.product_id join '. DB_PREFIX .'product_to_category pc on p.product_id = pc.product_id where 
                (pc.category_id = '. $cat_id .' and  p.manufacturer_id = "' . $manufacturer_id . '" and p.product_id = pd.product_id  AND pd.ptype = "' . $this->db->escape($ptype) . '") ORDER BY pd.year ASC
            ');
//            var_dump('Select distinct pd.year from ' . DB_PREFIX . 'product p JOIN ' . DB_PREFIX . 'product_description pd on p.product_id = pd.product_id join '. DB_PREFIX .'product_to_category pc on p.product_id = pc.product_id where
//            (pc.category_id = '. $cat_id .' and  p.manufacturer_id = "' . $manufacturer_id . '" and p.product_id = pd.product_id  AND pd.ptype = "' . $this->db->escape($ptype) . '"');
//            die();
        } else {
            $query = $this->db->query('Select distinct pd.year from ' . DB_PREFIX . 'product p JOIN ' . DB_PREFIX . 'product_description pd where 
             ( p.manufacturer_id = "' . $manufacturer_id . '" and p.product_id = pd.product_id  AND pd.ptype = "' . $this->db->escape($ptype) . '") ORDER BY pd.year ASC
            ');
        }

        foreach ($query->rows as $result) {
            $data ['years'][] = array(
                'year' => $result['year'],
                'url' => $this->url->link('product/options/models', "ptype=$ptype&brand_id=$manufacturer_id&year=" . $result['year'], true)
            );
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            $json['success'] = 'done';
            $json['years'] = $data['years'];
            $json['name'] = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id)['name'];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

        } else {
            $data['header'] = $this->load->controller('common/header');
            $data['footer'] = $this->load->controller('common/footer');
            $data['ptype'] = ucfirst($ptype);
            $this->response->setOutput($this->load->view('product/years', $data));
        }
    }

    public function models()
    {

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            $json = array();
            $manufacturer_id = $this->request->post['manu_id'];
            $year = $this->request->post['year'];
            $ptype = $this->request->post['ptype'];
            $cat_id = $this->request->post['cat_id'];

        } else {

            if (!isset($this->request->get['ptype']) && !isset($this->session->data['ptype'])) {
                $this->response->redirect($this->url->link('product/options', '', true));
            }
            $ptype = isset($this->request->get['ptype']) ? $this->request->get['ptype'] : $this->session->data['ptype'];
            $params = "ptype=$ptype";
            $this->session->data['ptype'] = $ptype;

            if (!isset($this->request->get['brand_id']) && !isset($this->session->data['manufacturer_id'])) {
                $this->response->redirect($this->url->link('product/options/manufactures', $params, true));
            }
            $manufacturer_id = isset($this->request->get['brand_id']) ? (int)$this->request->get['brand_id'] : $this->session->data['manufacturer_id'];
            $params .= "&brand_id=$manufacturer_id";
            $this->session->data['manufacturer_id'] = $manufacturer_id;

            if (!isset($this->request->get['year']) && !isset($this->session->data['car_year'])) {
                $this->response->redirect($this->url->link('product/options/years', $params, true));
            }
            $year = isset($this->request->get['year']) ? (int)$this->request->get['year'] : $this->session->data['car_year'];
            $this->session->data['car_year'] = $year;
        }

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        if (!empty($cat_id)) {

            $models = $this->db->query('Select distinct pd2.* , p.image from '. DB_PREFIX .'product_description pd join '. DB_PREFIX .'product_to_category pc on pd.product_id = pc.product_id AND pc.category_id = '. $cat_id .'
                                join '. DB_PREFIX .'product_description pd2 on pd2.product_id = pd.car_id join '.DB_PREFIX.'product p on p.product_id = pd2.product_id where pd.ptype="'. $ptype .'" AND pd2.year ='.$year)->rows;

        }else{
            $models = $this->db->query('Select distinct * from ' . DB_PREFIX . 'product_description pd join ' . DB_PREFIX . 'product p
             where (pd.product_id = p.product_id and  pd.ptype = "models" and p.manufacturer_id = ' . $manufacturer_id . ' and pd.year = ' . $year . ')')->rows;
        }

        foreach ($models as $key => $model) {
            $q = $this->db->query("Select * from " . DB_PREFIX . "product_description pd where pd.ptype = '$ptype' and pd.car_id = " . $model['product_id'])->rows;
            if (count($q) > 0) {
                $models[$key]['has_item'] = true;
                continue;
            } else
                $models[$key]['has_item'] = false;
        }


        foreach ($models as $model) {
            $edtion = $this->model_catalog_product->getProduct($model['product_id'])['filters'] > 0;
            if ($model['has_item']) {
                $data ['models'][] = array(
                    'id' => $model['product_id'],
                    'name' => $model['name'],
                    'year' => $model['year'],
                    'url' => $this->url->link('product/options/editions', "ptype=$ptype&brand_id=$manufacturer_id&year=$year&car_id=" . $model['product_id'], true),
                    'edtion' => $edtion,
                    'image' => $this->model_tool_image->resize($model['image'], 314, 105)
                );
            }
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            $json['success'] = 'done';
            $json['model'] = $data['models'];

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

        } else {
            $data['car_manufacturer'] = $this->session->data['car_manufacturer'];
            $data['year'] = $year;
            $data['ptype'] = ucfirst($ptype);
            $data['header'] = $this->load->controller('common/header');
            $data['footer'] = $this->load->controller('common/footer');


            $this->response->setOutput($this->load->view('product/models', $data));
        }
    }

    public function editions()
    {

        $this->load->model('catalog/product');
        $this->load->model('catalog/manufacturer');
        $this->load->model('tool/image');

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            $car_id = $this->request->post['model_id'];
            $year = $this->request->post['year'];
            $manufacturer_id = $this->request->post['manu_id'];
            $ptype = $this->request->post['ptype'];
            $cat_id = $this->request->post['cat_id'];

        } else {

            if (!isset($this->request->get['ptype']) && !isset($this->session->data['ptype'])) {
                $this->response->redirect($this->url->link('product/options', '', true));
            }
            $ptype = isset($this->request->get['ptype']) ? $this->request->get['ptype'] : $this->session->data['ptype'];
            $ptype = lcfirst($ptype);
            $params = "ptype=$ptype";
            $this->session->data['ptype'] = $ptype;

            if (!isset($this->request->get['brand_id']) && !isset($this->session->data['manufacturer_id'])) {
                $this->response->redirect($this->url->link('product/options/manufactures', $params, true));
            }
            $manufacturer_id = isset($this->request->get['brand_id']) ? (int)$this->request->get['brand_id'] : $this->session->data['manufacturer_id'];
            $params .= "&brand_id=$manufacturer_id";
            $this->session->data['manufacturer_id'] = $manufacturer_id;
            if (!isset($this->request->get['year']) && !isset($this->session->data['car_year'])) {
                $this->response->redirect($this->url->link('product/options/years', $params, true));
            }
            $year = isset($this->request->get['year']) ? (int)$this->request->get['year'] : $this->session->data['car_year'];
            $this->session->data['car_year'] = $year;
            $params .= "&year=$year";


            if (!isset($this->request->get['car_id']) && !isset($this->session->data['car_id'])) {
                $this->response->redirect($this->url->link('product/options/models', $params, true));
            }
            $car_id = isset($this->request->get['car_id']) ? (int)$this->request->get['car_id'] : $this->session->data['car_id'];
            $this->session->data['car_id'] = $car_id;

            $car_name = $this->model_catalog_product->getProduct($car_id)['name'];
            $this->session->data['car_name'] = $car_name;

            $man_name = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id)['name'];
            $this->session->data['car_manufacturer'] = $man_name;
            $data['car_name'] = $this->model_catalog_product->getProduct($car_id)['name'];
            $data['car_year'] = $year;
            $data['car_manufacturer'] = ucfirst($man_name);
            $data['car_image'] = $this->model_tool_image->resize($this->model_catalog_product->getProduct($car_id)['image'], 258, 111);
            $data['ptype'] = ucfirst($ptype);

            $ipAddress = $_SERVER['REMOTE_ADDR'];
            if (!$this->model_catalog_product->checkViewed($car_id, $ipAddress)) {
                $this->model_catalog_product->updateViewed($car_id, $ipAddress);
            }

        }

        if (!empty($cat_id)) {
            $q = "select distinct fd.* from cs_product_description pd join cs_product_edition pe on pd.product_id = pe.product_id join 
                cs_filter_description fd on fd.filter_id = pe.filter_id join cs_product_to_category pc on pd.product_id = pc.product_id where pc.category_id=$cat_id and pd.car_id = $car_id and pd.ptype = '$ptype' ";
        }else{
            $q = " select distinct fd.* from cs_product_description pd join cs_product_edition pe on pd.product_id = pe.product_id join 
                cs_filter_description fd on fd.filter_id = pe.filter_id where pd.car_id = $car_id and pd.ptype = '$ptype' ";
        }

        $editions = $this->db->query($q)->rows;
        $data['editions'] = array();
        foreach ($editions as $edition) {
            if ($this->request->server['REQUEST_METHOD'] == 'POST') {
                if (!empty($cat_id)) {
                    if($ptype == 'parts'){
                        $url = $this->url->link('product/' . $ptype . '/' . $ptype, "brand_id=$manufacturer_id&year=$year&car_id=$car_id&edition_id=" . $edition['filter_id'] . "&category_id=" . $cat_id, true);
                    }else{
                        $url = $this->url->link('product/' . $ptype, "brand_id=$manufacturer_id&year=$year&car_id=$car_id&edition_id=" . $edition['filter_id'] . "&category_id=" . $cat_id, true);
                    }
                } else {
                    $url = $this->url->link('product/' . $ptype, "brand_id=$manufacturer_id&year=$year&car_id=$car_id&edition_id=" . $edition['filter_id'], true);
                }
            } else {
                $url = '';
            }
            $data['editions'][] = array(
                'name' => $edition['name'],
                'edition_id' => $edition['filter_id'],
                'href' => $this->url->link('product/' . $ptype, "brand_id=$manufacturer_id&year=$year&car_id=$car_id&edition_id=" . $edition['filter_id'], true),
                'url' => $url
            );
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            $json['success'] = 'done';
            $json['editions'] = $data['editions'];
            $this->session->data['ptype'] = $ptype;

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));

        } else {


            if ($data['editions'] == null) {
                $this->session->data['edition_id'] = null;
                $this->response->redirect($this->url->link('product/' . $ptype, "brand_id=$manufacturer_id&year=$year&car_id=$car_id", true));
            } else {
                $data['car_id'] = $car_id;
                $data['header'] = $this->load->controller('common/header');
                $data['footer'] = $this->load->controller('common/footer');
                $data['change_vehicle'] = $this->load->controller('common/change_vehicle');

                $this->response->setOutput($this->load->view('product/editions', $data));
            }
        }
    }


}
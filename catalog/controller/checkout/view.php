<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/13/18
 * Time: 9:23 AM
 */

class ControllerCheckoutView extends Controller {
    public function index(){

    }

    public function add() {

        $json = array();

        if(!empty($this->session->data['view'] )){
            foreach ($this->session->data['view'] as $part){
                if($part['id'] == $this->request->post['part_id']){
                    $json['error'] = 'already exist';
                }
            }
        }

        if (isset($this->request->post['part_id'])) {
            $part_id = (int)$this->request->post['part_id'];
        } else {
            $part_id = 0;
        }


        if(empty($json['error'])){
            $this->load->model('catalog/product');

            $part_info = $this->model_catalog_product->getProduct($part_id);


            if ($part_info) {

                $this->session->data['view'][] = [
                    'id'            => $part_id,
                    'name'          => $part_info['name'],
                    'price'         => $part_info['price'],
                    'mpn'           => $part_info['mpn']
                ];

                $json['view'] = $this->session->data['view'];
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function clear(){
        $json = array();

        unset($this->session->data['view']);
        $json['success'] = 'clear';

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }
}
<?php
class ControllerCheckoutCheckout extends Controller {

    private $error = array();

	public function index() {
        unset($this->session->data['ptype']);

        $this->load->language('account/edit');
        $this->load->language('account/address');
        $this->load->model('account/address');

        if(($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()){
            
            $shipping_address_info = $this->model_account_address->getAddressByType('shipping');

            $address = array(
                'firstname' => $this->request->post['billing_firstname'],
                'lastname' => $this->request->post['billing_lastname'],
                'address_1' => $this->request->post['billing_line1'],
                'address_2' => $this->request->post['billing_line2'],
                'company' => '',
                'postcode' => $this->request->post['billing_zip'],
                'zone_id' => $this->request->post['billing_zone_id'],
                'country_id' => $this->request->post['billing_country_id'],
                'city' => $this->request->post['billing_city'],
                'type' => 'billing',
                'apt'   => ''

            );
            
            if(!isset($this->request->post['shipping_same'])) {
                $address = array(
                    'firstname' => $this->request->post['shipping_firstname'],
                    'lastname' => $this->request->post['shipping_lastname'],
                    'address_1' => $this->request->post['shipping_line1'],
                    'address_2' => $this->request->post['shipping_line2'],
                    'company' => '',
                    'postcode' => $this->request->post['shipping_zip'],
                    'zone_id' => $this->request->post['shipping_zone_id'],
                    'country_id' => $this->request->post['shipping_country_id'],
                    'city' => $this->request->post['shipping_city'],
                    'type' => 'shipping'

                );
            }
            if(!$shipping_address_info){
                $this->model_account_address->addAddress($this->customer->getId(), $address);
            }
            else{
                $this->model_account_address->editAddress($shipping_address_info['address_id'],$address);
            }

            $this->placeOrder();
            $this->session->data['success'] = $this->language->get('text_success');
            if($this->customer->isLogged()){
                $this->response->redirect($this->url->link('account/account','', true));
            }
            else {
                $this->response->redirect($this->url->link('checkout/checkout', '', true));
            }

        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['billing_firstname'])) {
            $data['error_billing_firstname'] = $this->error['billing_firstname'];
        } else {
            $data['error_billing_firstname'] = '';
        }

        if (isset($this->error['billing_lastname'])) {
            $data['error_billing_lastname'] = $this->error['billing_lastname'];
        } else {
            $data['error_billing_lastname'] = '';
        }

        if (isset($this->error['billing_email'])) {
            $data['error_billing_email'] = $this->error['billing_email'];
        } else {
            $data['error_billing_email'] = '';
        }

        if (isset($this->error['shipping_firstname'])) {
            $data['error_shipping_firstname'] = $this->error['shipping_firstname'];
        } else {
            $data['error_shipping_firstname'] = '';
        }

        if (isset($this->error['shipping_lastname'])) {
            $data['error_shipping_lastname'] = $this->error['shipping_lastname'];
        } else {
            $data['error_shipping_lastname'] = '';
        }

        if (isset($this->error['shipping_email'])) {
            $data['error_shipping_email'] = $this->error['shipping_email'];
        } else {
            $data['error_shipping_email'] = '';
        }


        if (isset($this->error['shipping_city'])) {
            $data['error_shipping_city'] = $this->error['shipping_city'];
        } else {
            $data['error_shipping_city'] = '';
        }

        if (isset($this->error['shipping_postcode'])) {
            $data['error_shipping_zip'] = $this->error['shipping_zip'];
        } else {
            $data['error_shipping_zip'] = '';
        }

        if (isset($this->error['shipping_country'])) {
            $data['error_shipping_country'] = $this->error['shipping_country'];
        } else {
            $data['error_shipping_country'] = '';
        }

        if (isset($this->error['shipping_zone'])) {
            $data['error_shipping_zone'] = $this->error['shipping_zone'];
        } else {
            $data['error_shipping_zone'] = '';
        }

        if (isset($this->error['billing_address'])) {
            $data['error_billing_address'] = $this->error['billing_address'];
        } else {
            $data['error_billing_address'] = '';
        }

        if (isset($this->error['billing_city'])) {
            $data['error_billing_city'] = $this->error['billing_city'];
        } else {
            $data['error_billing_city'] = '';
        }

        if (isset($this->error['billing_postcode'])) {
            $data['error_billing_zip'] = $this->error['billing_zip'];
        } else {
            $data['error_billing_zip'] = '';
        }

        if (isset($this->error['billing_country'])) {
            $data['error_billing_country'] = $this->error['billing_country'];
        } else {
            $data['error_billing_country'] = '';
        }

        if (isset($this->error['billing_zone'])) {
            $data['error_billing_zone'] = $this->error['billing_zone'];
        } else {
            $data['error_billing_zone'] = '';
        }

        if (isset($this->error['custom_field'])) {
            $data['error_custom_field'] = $this->error['custom_field'];
        } else {
            $data['error_custom_field'] = array();
        }

		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$this->response->redirect($this->url->link('checkout/cart'));
			}
		}
        $this->load->model('catalog/product');
		$this->load->language('checkout/checkout');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['text_checkout_option'] = sprintf($this->language->get('text_checkout_option'), 1);
		$data['text_checkout_account'] = sprintf($this->language->get('text_checkout_account'), 2);
		$data['text_checkout_payment_address'] = sprintf($this->language->get('text_checkout_payment_address'), 2);
		$data['text_checkout_shipping_address'] = sprintf($this->language->get('text_checkout_shipping_address'), 3);
		$data['text_checkout_shipping_method'] = sprintf($this->language->get('text_checkout_shipping_method'), 4);
		
		if ($this->cart->hasShipping()) {
			$data['text_checkout_payment_method'] = sprintf($this->language->get('text_checkout_payment_method'), 5);
			$data['text_checkout_confirm'] = sprintf($this->language->get('text_checkout_confirm'), 6);
		} else {
			$data['text_checkout_payment_method'] = sprintf($this->language->get('text_checkout_payment_method'), 3);
			$data['text_checkout_confirm'] = sprintf($this->language->get('text_checkout_confirm'), 4);	
		}

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$data['error_warning'] = '';
		}

		$data['logged'] = $this->customer->isLogged();

		if (isset($this->session->data['account'])) {
			$data['account'] = $this->session->data['account'];
		} else {
			$data['account'] = '';
		}

		$data['shipping_required'] = $this->cart->hasShipping();
        $data['cart']  = $this->load->controller('checkout/cart');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['payment_address'] = $this->load->controller('checkout/payment_address');
		$data['shipping_address'] = $this->load->controller('checkout/shipping_address');
		$data['action'] = $this->url->link('checkout/checkout','',true);
		$data['vins'] = $this->model_catalog_product->getVINNumbers();

		$this->response->setOutput($this->load->view('checkout/checkout', $data));
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    protected function validate() {
        if ((utf8_strlen(trim($this->request->post['billing_firstname'])) < 1) || (utf8_strlen(trim($this->request->post['billing_firstname'])) > 32)) {
            $this->error['billing_firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['billing_lastname'])) < 1) || (utf8_strlen(trim($this->request->post['billing_lastname'])) > 32)) {
            $this->error['billing_lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['billing_email']) > 96) || !filter_var($this->request->post['billing_email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['billing_email'] = $this->language->get('error_email');
        }

        if (($this->customer->getEmail() != $this->request->post['billing_email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['billing_email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }


        if ((utf8_strlen(trim($this->request->post['billing_line1'])) < 3) || (utf8_strlen(trim($this->request->post['billing_line1'])) > 128)) {
            $this->error['billing_line1'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen(trim($this->request->post['billing_line2'])) < 3) || (utf8_strlen(trim($this->request->post['billing_line2'])) > 128)) {
            $this->error['billing_line2'] = $this->language->get('error_address_1');
        }

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->post['billing_country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['billing_zip'])) < 2 || utf8_strlen(trim($this->request->post['billing_zip'])) > 10)) {
            $this->error['billing_zip'] = $this->language->get('error_postcode');
        }

        if ($this->request->post['billing_country_id'] == '' || !is_numeric($this->request->post['billing_country_id'])) {
            $this->error['billing_country'] = $this->language->get('error_country');
        }

        if (!isset($this->request->post['billing_zone_id']) || $this->request->post['billing_zone_id'] == '' || !is_numeric($this->request->post['billing_zone_id'])) {
            $this->error['billing_zone'] = $this->language->get('error_zone');
        }

        if ((utf8_strlen(trim($this->request->post['billing_city'])) < 2) || (utf8_strlen(trim($this->request->post['billing_city'])) > 128)) {
            $this->error['billing_city'] = $this->language->get('error_city');
        }

        if(!isset($this->request->post['shipping_same'])){

            if ((utf8_strlen(trim($this->request->post['shipping_firstname'])) < 1) || (utf8_strlen(trim($this->request->post['shipping_firstname'])) > 32)) {
                $this->error['shipping_firstname'] = $this->language->get('error_firstname');
            }

            if ((utf8_strlen(trim($this->request->post['shipping_lastname'])) < 1) || (utf8_strlen(trim($this->request->post['shipping_lastname'])) > 32)) {
                $this->error['shipping_lastname'] = $this->language->get('error_lastname');
            }

            if ((utf8_strlen($this->request->post['shipping_email']) > 96) || !filter_var($this->request->post['shipping_email'], FILTER_VALIDATE_EMAIL)) {
                $this->error['shipping_email'] = $this->language->get('error_email');
            }

            if (($this->customer->getEmail() != $this->request->post['shipping_email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['shipping_email'])) {
                $this->error['warning'] = $this->language->get('error_exists');
            }


            if ((utf8_strlen(trim($this->request->post['shipping_line1'])) < 3) || (utf8_strlen(trim($this->request->post['shipping_line1'])) > 128)) {
                $this->error['shipping_line1'] = $this->language->get('error_address_1');
            }

            if ((utf8_strlen(trim($this->request->post['shipping_line2'])) < 3) || (utf8_strlen(trim($this->request->post['shipping_line2'])) > 128)) {
                $this->error['shipping_line2'] = $this->language->get('error_address_1');
            }


            $this->load->model('localisation/country');

            $country_info = $this->model_localisation_country->getCountry($this->request->post['shipping_country_id']);

            if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['shipping_zip'])) < 2 || utf8_strlen(trim($this->request->post['shipping_zip'])) > 10)) {
                $this->error['shipping_zip'] = $this->language->get('error_postcode');
            }

            if ($this->request->post['shipping_country_id'] == '' || !is_numeric($this->request->post['shipping_country_id'])) {
                $this->error['shipping_country'] = $this->language->get('error_country');
            }

            if (!isset($this->request->post['shipping_zone_id']) || $this->request->post['shipping_zone_id'] == '' || !is_numeric($this->request->post['shipping_zone_id'])) {
                $this->error['shipping_zone'] = $this->language->get('error_zone');
            }

            if ((utf8_strlen(trim($this->request->post['shipping_city'])) < 2) || (utf8_strlen(trim($this->request->post['shipping_city'])) > 128)) {
                $this->error['shipping_city'] = $this->language->get('error_city');
            }
        }

        return !$this->error;
    }
    
    
    
    private function placeOrder(){

        $order_data = array();

        $totals = array();
        $taxes = $this->cart->getTaxes();
        $total = 0;

        // Because __call can not keep var references so we put them into an array.
        $total_data = array(
            'totals' => &$totals,
            'taxes'  => &$taxes,
            'total'  => &$total
        );

        $this->load->model('setting/extension');

        $sort_order = array();

        $results = $this->model_setting_extension->getExtensions('total');

        foreach ($results as $key => $value) {
            $sort_order[$key] = $this->config->get('total_' . $value['code'] . '_sort_order');
        }

        array_multisort($sort_order, SORT_ASC, $results);

        foreach ($results as $result) {
            if ($this->config->get('total_' . $result['code'] . '_status')) {
                $this->load->model('extension/total/' . $result['code']);

                // We have to put the totals in an array so that they pass by reference.
                $this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
            }
        }

        $sort_order = array();

        foreach ($totals as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $totals);

        $order_data['totals'] = $totals;

        $this->load->language('checkout/checkout');

        $order_data['invoice_prefix'] = $this->config->get('config_invoice_prefix');
        $order_data['store_id'] = $this->config->get('config_store_id');
        $order_data['store_name'] = $this->config->get('config_name');

        if ($order_data['store_id']) {
            $order_data['store_url'] = $this->config->get('config_url');
        } else {
            if ($this->request->server['HTTPS']) {
                $order_data['store_url'] = HTTPS_SERVER;
            } else {
                $order_data['store_url'] = HTTP_SERVER;
            }
        }
        $this->load->model('account/customer');
        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');

        if ($this->customer->isLogged()) {
            $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

            $order_data['customer_id'] = $this->customer->getId();
            $order_data['customer_group_id'] = $customer_info['customer_group_id'];
            $order_data['firstname'] = $customer_info['firstname'];
            $order_data['lastname'] = $customer_info['lastname'];
            $order_data['email'] = $customer_info['email'];
            $order_data['telephone'] = $customer_info['telephone'];
            $order_data['custom_field'] = json_decode($customer_info['custom_field'], true);
        } elseif (isset($this->session->data['guest'])) {
            $order_data['customer_id'] = 0;
            $order_data['customer_group_id'] = $this->session->data['guest']['customer_group_id'];
            $order_data['firstname'] = $this->session->data['guest']['firstname'];
            $order_data['lastname'] = $this->session->data['guest']['lastname'];
            $order_data['email'] = $this->session->data['guest']['email'];
            $order_data['telephone'] = $this->session->data['guest']['telephone'];
            $order_data['custom_field'] = $this->session->data['guest']['custom_field'];
        }

        $country = $this->model_localisation_country->getCountry($this->request->post['billing_country_id']);

        $order_data['payment_firstname'] = $this->request->post['billing_firstname'];
        $order_data['payment_lastname'] = $this->request->post['billing_lastname'];
        $order_data['payment_company'] = '';
        $order_data['payment_address_1'] = $this->request->post['billing_line1'];
        $order_data['payment_address_2'] = $this->request->post['billing_line2'];
        $order_data['payment_city'] = $this->request->post['billing_city'];
        $order_data['payment_postcode'] = $this->request->post['billing_zip'];
        $order_data['payment_zone'] = $this->model_localisation_zone->getZone($this->request->post['billing_zone_id'])['name'];
        $order_data['payment_zone_id'] = $this->request->post['billing_zone_id'];
        $order_data['payment_country'] = $country['name'];
        $order_data['payment_country_id'] = $this->request->post['billing_country_id'];
        $order_data['payment_address_format'] = $country['address_format'];
        $order_data['payment_custom_field'] = array();

        if (isset($this->session->data['payment_method']['title'])) {
            $order_data['payment_method'] = $this->session->data['payment_method']['title'];
        } else {
            $order_data['payment_method'] = '';
        }

        if (isset($this->session->data['payment_method']['code'])) {
            $order_data['payment_code'] = $this->session->data['payment_method']['code'];
        } else {
            $order_data['payment_code'] = '';
        }

        if (!isset($this->request->post['shipping_same'])) {
            $country = $this->model_localisation_country->getCountry($this->request->post['shipping_country_id']);

            $order_data['shipping_firstname'] = $this->request->post['shipping_firstname'];
            $order_data['shipping_lastname'] = $this->request->post['shipping_lastname'];
            $order_data['shipping_company'] = '';
            $order_data['shipping_address_1'] = $this->request->post['shipping_line1'];
            $order_data['shipping_address_2'] = $this->request->post['shipping_line2'];
            $order_data['shipping_city'] = $this->request->post['shipping_city'];
            $order_data['shipping_postcode'] = $this->request->post['shipping_zip'];
            $order_data['shipping_zone'] = $this->model_localisation_zone->getZone($this->request->post['shipping_zone_id'])['name'];
            $order_data['shipping_zone_id'] = $this->request->post['shipping_zone_id'];
            $order_data['shipping_country'] = $country['name'];
            $order_data['shipping_country_id'] = $this->request->post['shipping_country_id'];
            $order_data['shipping_address_format'] = $country['address_format'];
            $order_data['shipping_custom_field'] = array();

            if (isset($this->session->data['shipping_method']['title'])) {
                $order_data['shipping_method'] = $this->session->data['shipping_method']['title'];
            } else {
                $order_data['shipping_method'] = '';
            }

            if (isset($this->session->data['shipping_method']['code'])) {
                $order_data['shipping_code'] = $this->session->data['shipping_method']['code'];
            } else {
                $order_data['shipping_code'] = '';
            }
        } else {
            $country = $this->model_localisation_country->getCountry($this->request->post['billing_country_id']);

            $order_data['shipping_firstname'] = $this->request->post['billing_firstname'];
            $order_data['shipping_lastname'] = $this->request->post['billing_lastname'];
            $order_data['shipping_company'] = '';
            $order_data['shipping_address_1'] = $this->request->post['billing_line1'];
            $order_data['shipping_address_2'] = $this->request->post['billing_line2'];
            $order_data['shipping_city'] = $this->request->post['billing_city'];
            $order_data['shipping_postcode'] = $this->request->post['billing_zip'];
            $order_data['shipping_zone'] = $this->model_localisation_zone->getZone($this->request->post['billing_zone_id'])['name'];
            $order_data['shipping_zone_id'] = $this->request->post['billing_zone_id'];
            $order_data['shipping_country'] = $country['name'];
            $order_data['shipping_country_id'] = $this->request->post['billing_country_id'];
            $order_data['shipping_address_format'] = $country['address_format'];
            $order_data['shipping_custom_field'] = array();
            $order_data['shipping_method'] = '';
            $order_data['shipping_code'] = '';
        }

        $order_data['products'] = array();

        foreach ($this->cart->getProducts() as $product) {
            $option_data = array();

            foreach ($product['option'] as $option) {
                $option_data[] = array(
                    'product_option_id'       => $option['product_option_id'],
                    'product_option_value_id' => $option['product_option_value_id'],
                    'option_id'               => $option['option_id'],
                    'option_value_id'         => $option['option_value_id'],
                    'name'                    => $option['name'],
                    'value'                   => $option['value'],
                    'type'                    => $option['type']
                );
            }

            $order_data['products'][] = array(
                'product_id' => $product['product_id'],
                'name'       => $product['name'],
                'model'      => $product['model'],
                'option'     => $option_data,
                'download'   => $product['download'],
                'quantity'   => $product['quantity'],
                'subtract'   => $product['subtract'],
                'price'      => $product['price'],
                'total'      => $product['total'],
                'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
                'reward'     => $product['reward']
            );
        }

        // Gift Voucher
        $order_data['vouchers'] = array();

        if (!empty($this->session->data['vouchers'])) {
            foreach ($this->session->data['vouchers'] as $voucher) {
                $order_data['vouchers'][] = array(
                    'description'      => $voucher['description'],
                    'code'             => token(10),
                    'to_name'          => $voucher['to_name'],
                    'to_email'         => $voucher['to_email'],
                    'from_name'        => $voucher['from_name'],
                    'from_email'       => $voucher['from_email'],
                    'voucher_theme_id' => $voucher['voucher_theme_id'],
                    'message'          => $voucher['message'],
                    'amount'           => $voucher['amount']
                );
            }
        }

        $order_data['comment'] = $this->request->post['comment'];
        $order_data['total'] = $total_data['total'];

        if (isset($this->request->cookie['tracking'])) {
            $order_data['tracking'] = $this->request->cookie['tracking'];

            $subtotal = $this->cart->getSubTotal();

            // Affiliate
            $affiliate_info = $this->model_account_customer->getAffiliateByTracking($this->request->cookie['tracking']);

            if ($affiliate_info) {
                $order_data['affiliate_id'] = $affiliate_info['customer_id'];
                $order_data['commission'] = ($subtotal / 100) * $affiliate_info['commission'];
            } else {
                $order_data['affiliate_id'] = 0;
                $order_data['commission'] = 0;
            }

            // Marketing
            $this->load->model('checkout/marketing');

            $marketing_info = $this->model_checkout_marketing->getMarketingByCode($this->request->cookie['tracking']);

            if ($marketing_info) {
                $order_data['marketing_id'] = $marketing_info['marketing_id'];
            } else {
                $order_data['marketing_id'] = 0;
            }
        } else {
            $order_data['affiliate_id'] = 0;
            $order_data['commission'] = 0;
            $order_data['marketing_id'] = 0;
            $order_data['tracking'] = '';
        }

        $order_data['language_id'] = $this->config->get('config_language_id');
        $order_data['currency_id'] = $this->currency->getId($this->session->data['currency']);
        $order_data['currency_code'] = $this->session->data['currency'];
        $order_data['currency_value'] = $this->currency->getValue($this->session->data['currency']);
        $order_data['ip'] = $this->request->server['REMOTE_ADDR'];

        if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->request->server['HTTP_CLIENT_IP'])) {
            $order_data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
        } else {
            $order_data['forwarded_ip'] = '';
        }

        if (isset($this->request->server['HTTP_USER_AGENT'])) {
            $order_data['user_agent'] = $this->request->server['HTTP_USER_AGENT'];
        } else {
            $order_data['user_agent'] = '';
        }

        if (isset($this->request->server['HTTP_ACCEPT_LANGUAGE'])) {
            $order_data['accept_language'] = $this->request->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $order_data['accept_language'] = '';
        }

        $this->load->model('checkout/order');

        $this->session->data['order_id'] = $this->model_checkout_order->addOrder($order_data);

    }
    
}
<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 8/12/18
 * Time: 9:56 AM
 */

class ControllerCheckoutGarage extends Controller {

    public function index() {

        $data = array();

        $data['count'] = 1;
//        $this->response->setOutput($this->load->view('checkout/garage', $data));
    }

    public function add() {

        $json = array();

        if($this->customer->isLogged()){

            $this->load->model('account/garage');
            $fake = $this->model_account_garage->getGarage($this->customer->getid());

            foreach ($fake as $car){
                if($car['id'] == $this->request->post['car_id']){
                    $json['error'] = 'already exist at database';
                }
            }
        }


        if(!empty($this->session->data['garage'] )){
            foreach ($this->session->data['garage'] as $car){
                if($car['id'] == $this->request->post['car_id']){
                    $json['error'] = 'already exist';
                }
            }
        }

        if (isset($this->request->post['car_id'])) {
            $car_id     = (int)$this->request->post['car_id'];
        } else {
            $car_id = 0;
        }

        if(isset($this->request->post['edition_id'])){
            $edition_id = (int)$this->request->post['edition_id'];

            $q = "select  fd.name from " . DB_PREFIX . "filter_description fd where fd.filter_id = $edition_id ";
            $edition = $this->db->query($q)->row;

        }else{
            $edition_id = 0;
            $edition = 0;
        }

        if(empty($json['error'])){
            $this->load->model('catalog/product');

            $car_info = $this->model_catalog_product->getProduct($car_id);

            if ($car_info) {

                if($edition){
                    $edtion_name = $edition['name'];
                }else{
                    $edtion_name = '';
                }

                $this->session->data['garage'][] = [
                    'id'            => $car_id,
                    'name'          => $car_info['name'],
                    'image'         => $car_info['image'],
                    'year'          => $car_info['year'],
                    'manufacturer'  => $car_info['manufacturer'],
                    'edition_id'    => $edition_id,
                    'edtion_name'   => $edtion_name
                ];

                $json['garage'] = $this->session->data['garage'];
                $this->session->data['car_id']              = $this->session->data['garage'][count($this->session->data['garage'])-1]['id'];
                $this->session->data['car_name']            = $this->session->data['garage'][count($this->session->data['garage'])-1]['name'];
                $this->session->data['car_image']           = $this->session->data['garage'][count($this->session->data['garage'])-1]['image'];
                $this->session->data['car_year']            = $this->session->data['garage'][count($this->session->data['garage'])-1]['year'];
                $this->session->data['car_manufacturer']    = $this->session->data['garage'][count($this->session->data['garage'])-1]['manufacturer'];
                $this->session->data['edition_id']          = $this->session->data['garage'][count($this->session->data['garage'])-1]['edition_id'];
            }
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function remove(){

        if (isset($this->request->post['car_id'])) {
            $car_id = (int)$this->request->post['car_id'];
        } else {
            $car_id = 0;
        }


        $this->load->model('account/garage');
        $fake = $this->model_account_garage->removeFromGarage($this->customer->getid(), $car_id);
        $json = $fake;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function save(){

        if (isset($this->request->post['car_id'])) {
            $car_id = (int)$this->request->post['car_id'];
        } else {
            $car_id = 0;
        }

        $this->load->model('account/garage');
        $fake = $this->model_account_garage->addToGarage($this->customer->getid(), $car_id);
        $json = $fake;

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function clear(){
        $json = array();

        unset($this->session->data['garage']);
        unset($this->session->data['car_id']);
        $json['success'] = 'clear';

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

}
<?php
class ControllerExtensionModuleBanner extends Controller {
	public function index($setting) {
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');



		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		foreach ($results as $result) {

            $det = $result['title'];
            $title = explode('/', $det);

            if(count($title) > 1){
                $title1 = $title[0];
                $title2 = $title[1];
                $title3 = $title[2];

                if (is_file(DIR_IMAGE . $result['image'])) {
                    $data['banners'][] = array(
                        'title1'        => $title1,
                        'title2'        => $title2,
                        'title3'        => $title3,
                        'link'          => $result['link'],
                        'description'   => $result['description'],
                        'image'         => $this->model_tool_image->resize($result['image'], 610, 457)
                    );
                }

            }else{
                if (is_file(DIR_IMAGE . $result['image'])) {
                    $data['banners'][] = array(
                        'title1'        => $result['title'],
                        'link'          => $result['link'],
                        'description'   => $result['description'],
                        'image'         => $this->model_tool_image->resize($result['image'], 610, 457)
                    );
                }
            }


		}

		$data['module'] = $module++;



		return $this->load->view('extension/module/banner', $data);
	}
}
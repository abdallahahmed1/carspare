<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 12/08/18
 * Time: 06:33 م
 */

class ControllerAccountGarage extends Controller
{
    public function index()
    {
        $this->load->model('account/garage');
        $data = array();
        $cars = $this->model_account_garage->getGarage($this->customer->getId());
        foreach($cars as $car){
            $data['cars'][] = array(
               'name'               => $car['name'],
               'manufacturer'       => $car['manufacturer'],
               'year'               => $car['year'],
               'accessories_href'   => $this->url->link('product/accessories','car_id='.$car['product_id'])
            ) ;
        }
        return $this->load->view('account/garage', $data);
    }

    public function add()
    {
        if(!isset($this->request->post['product_id']) || !$this->customer->isLogged()){
            return false;
        }
        $this->load->model('account/garage');
        $customer_id = $this->customer->getId();
        $product_id = $this->request->post['product_id'];
        $this->model_account_garage->addToGarage($customer_id,$product_id);
        return true;
    }

    public function remove()
    {
        if(!isset($this->request->post['product_id']) || !$this->customer->isLogged()){
            return false;
        }
        $this->load->model('account/garage');
        $customer_id = $this->customer->getId();
        $product_id = $this->request->post['product_id'];
        $this->model_account_garage->removeFromGarage($customer_id,$product_id);
        return true;
    }
}
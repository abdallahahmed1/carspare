<?php
class ControllerAccountEdit extends Controller {
	private $error = array();

	public function index() {


		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

        $this->load->language('account/edit');
        $this->load->language('account/address');

        $this->load->model('account/customer');
        $this->load->model('account/address');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['firstname']) && $this->validate()) {
            $this->model_account_customer->editCustomer($this->customer->getId(), $this->request->post);
            $general_address_info = $this->model_account_address->getAddressByType('general');
            $address = array(
                'firstname' => $this->request->post['firstname'],
                'lastname' => $this->request->post['lastname'],
                'address_1' => $this->request->post['general_address'],
                'address_2' => '',
                'company'   => '',
                'postcode'  => $this->request->post['general_postcode'],
                'zone_id'  => $this->request->post['general_zone_id'],
                'country_id'  => $this->request->post['general_country_id'],
                'city'  => $this->request->post['general_city'],
                'type'  => 'general',
                'apt'   => $this->request->post['general_apt']

            );
            if(!$general_address_info){
                $this->model_account_address->addAddress($this->customer->getId(), $address);
            }
            else{
                $this->model_account_address->editAddress($general_address_info['address_id'],$address);
            }


            $billing_address_info = $this->model_account_address->getAddressByType('billing');

            $address = array(
                'firstname' => $this->request->post['firstname'],
                'lastname' => $this->request->post['lastname'],
                'address_1' => $this->request->post['billing_address'],
                'address_2' => '',
                'company'   => '',
                'postcode'  => $this->request->post['billing_postcode'],
                'zone_id'  => $this->request->post['billing_zone_id'],
                'country_id'  => $this->request->post['billing_country_id'],
                'city'  => $this->request->post['billing_city'],
                'type'  => 'billing',
                'apt'   => $this->request->post['billing_apt']
            );
            if(!$billing_address_info){
                $this->model_account_address->addAddress($this->customer->getId(), $address);
            }
            else{
                $this->model_account_address->editAddress($billing_address_info['address_id'],$address);
            }

            if(utf8_strlen($this->request->post['old_password']) > 0 || utf8_strlen($this->request->post['password']) > 0 ) {
                $this->model_account_customer->editPassword($this->customer->getEmail(), $this->request->post['password']);
            }

            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('account/account', 'tab=settings', true));

        }


        $dataEdit['breadcrumbs'] = array();

        $dataEdit['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $dataEdit['breadcrumbs'][] = array(
            'text' => $this->language->get('text_account'),
            'href' => $this->url->link('account/account', '', true)
        );

        $dataEdit['breadcrumbs'][] = array(
            'text' => $this->language->get('text_edit'),
            'href' => $this->url->link('account/edit', '', true)
        );

        if (isset($this->error['warning'])) {
            $dataEdit['error_warning'] = $this->error['warning'];
        } else {
            $dataEdit['error_warning'] = '';
        }

        if (isset($this->error['firstname'])) {
            $dataEdit['error_firstname'] = $this->error['firstname'];
        } else {
            $dataEdit['error_firstname'] = '';
        }

        if (isset($this->error['lastname'])) {
            $dataEdit['error_lastname'] = $this->error['lastname'];
        } else {
            $dataEdit['error_lastname'] = '';
        }

        if (isset($this->error['email'])) {
            $dataEdit['error_email'] = $this->error['email'];
        } else {
            $dataEdit['error_email'] = '';
        }

        if (isset($this->error['custom_field'])) {
            $dataEdit['error_custom_field'] = $this->error['custom_field'];
        } else {
            $dataEdit['error_custom_field'] = array();
        }

        if (isset($this->error['general_address'])) {
            $dataEdit['error_general_address'] = $this->error['general_address'];
        } else {
            $dataEdit['error_general_address'] = '';
        }

        if (isset($this->error['general_city'])) {
            $dataEdit['error_general_city'] = $this->error['general_city'];
        } else {
            $dataEdit['error_general_city'] = '';
        }

        if (isset($this->error['general_postcode'])) {
            $dataEdit['error_general_postcode'] = $this->error['general_postcode'];
        } else {
            $dataEdit['error_general_postcode'] = '';
        }

        if (isset($this->error['general_country'])) {
            $dataEdit['error_general_country'] = $this->error['general_country'];
        } else {
            $dataEdit['error_general_country'] = '';
        }

        if (isset($this->error['general_zone'])) {
            $dataEdit['error_general_zone'] = $this->error['general_zone'];
        } else {
            $dataEdit['error_general_zone'] = '';
        }

        if (isset($this->error['billing_address'])) {
            $dataEdit['error_billing_address'] = $this->error['billing_address'];
        } else {
            $dataEdit['error_billing_address'] = '';
        }

        if (isset($this->error['billing_city'])) {
            $dataEdit['error_billing_city'] = $this->error['billing_city'];
        } else {
            $dataEdit['error_billing_city'] = '';
        }

        if (isset($this->error['billing_postcode'])) {
            $dataEdit['error_billing_postcode'] = $this->error['billing_postcode'];
        } else {
            $dataEdit['error_billing_postcode'] = '';
        }

        if (isset($this->error['billing_country'])) {
            $dataEdit['error_billing_country'] = $this->error['billing_country'];
        } else {
            $dataEdit['error_billing_country'] = '';
        }

        if (isset($this->error['billing_zone'])) {
            $dataEdit['error_billing_zone'] = $this->error['billing_zone'];
        } else {
            $dataEdit['error_billing_zone'] = '';
        }

        if (isset($this->error['custom_field'])) {
            $dataEdit['error_custom_field'] = $this->error['custom_field'];
        } else {
            $dataEdit['error_custom_field'] = array();
        }


        //password
        if (isset($this->error['old_password'])) {
            $dataEdit['error_old_password'] = $this->error['old_password'];
        } else {
            $dataEdit['error_old_password'] = '';
        }

        if (isset($this->error['password'])) {
            $dataEdit['error_password'] = $this->error['password'];
        } else {
            $dataEdit['error_password'] = '';
        }

        if (isset($this->error['confirm'])) {
            $dataEdit['error_confirm'] = $this->error['confirm'];
        } else {
            $dataEdit['error_confirm'] = '';
        }

        if (isset($this->request->post['old_password'])) {
            $dataEdit['old_password'] = $this->request->post['old_password'];
        } else {
            $dataEdit['old_password'] = '';
        }

        if (isset($this->request->post['password'])) {
            $dataEdit['password'] = $this->request->post['password'];
        } else {
            $dataEdit['password'] = '';
        }

        if (isset($this->request->post['confirm'])) {
            $dataEdit['confirm'] = $this->request->post['confirm'];
        } else {
            $dataEdit['confirm'] = '';
        }

        $dataEdit['action'] = $this->url->link('account/account', '', true);

        if ($this->request->server['REQUEST_METHOD'] != 'POST') {
            $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
            $general_address_info = $this->model_account_address->getAddressByType('general');
            $billing_address_info = $this->model_account_address->getAddressByType('billing');
        }

        if (isset($this->request->post['firstname'])) {
            $dataEdit['firstname'] = $this->request->post['firstname'];
        } elseif (!empty($customer_info)) {
            $dataEdit['firstname'] = $customer_info['firstname'];
        } else {
            $dataEdit['firstname'] = '';
        }

        if (isset($this->request->post['lastname'])) {
            $dataEdit['lastname'] = $this->request->post['lastname'];
        } elseif (!empty($customer_info)) {
            $dataEdit['lastname'] = $customer_info['lastname'];
        } else {
            $dataEdit['lastname'] = '';
        }

        if (isset($this->request->post['email'])) {
            $dataEdit['email'] = $this->request->post['email'];
        } elseif (!empty($customer_info)) {
            $dataEdit['email'] = $customer_info['email'];
        } else {
            $dataEdit['email'] = '';
        }

        if (isset($this->request->post['general_address'])) {
            $dataEdit['general_address'] = $this->request->post['general_address'];
        } elseif (!empty($general_address_info)) {
            $dataEdit['general_address'] = $general_address_info['address_1'];
        } else {
            $dataEdit['general_address'] = '';
        }

        if (isset($this->request->post['general_apt'])) {
            $dataEdit['general_apt'] = $this->request->post['general_apt'];
        } elseif (!empty($general_address_info)) {
            $dataEdit['general_apt'] = $general_address_info['apt_number'];
        } else {
            $dataEdit['general_apt'] = '';
        }


        if (isset($this->request->post['general_postcode'])) {
            $dataEdit['general_postcode'] = $this->request->post['general_postcode'];
        } elseif (!empty($general_address_info)) {
            $dataEdit['general_postcode'] = $general_address_info['postcode'];
        } else {
            $dataEdit['general_postcode'] = '';
        }

        if (isset($this->request->post['general_city'])) {
            $dataEdit['general_city'] = $this->request->post['general_city'];
        } elseif (!empty($general_address_info)) {
            $dataEdit['general_city'] = $general_address_info['city'];
        } else {
            $dataEdit['general_city'] = '';
        }

        if (isset($this->request->post['general_country_id'])) {
            $dataEdit['general_country_id'] = (int)$this->request->post['general_country_id'];
        }  elseif (!empty($general_address_info)) {
            $dataEdit['general_country_id'] = $general_address_info['country_id'];
        } else {
            $dataEdit['general_country_id'] = $this->config->get('config_country_id');
        }

        if (isset($this->request->post['general_zone_id'])) {
            $dataEdit['general_zone_id'] = (int)$this->request->post['general_zone_id'];
        }  elseif (!empty($general_address_info)) {
            $dataEdit['general_zone_id'] = $general_address_info['zone_id'];
        } else {
            $dataEdit['general_zone_id'] = '';
        }


        //billing
        if (isset($this->request->post['billing_address'])) {
            $dataEdit['billing_address'] = $this->request->post['billing_address'];
        } elseif (!empty($billing_address_info)) {
            $dataEdit['billing_address'] = $billing_address_info['address_1'];
        } else {
            $dataEdit['billing_address'] = '';
        }

        if (isset($this->request->post['billing_apt'])) {
            $dataEdit['billing_apt'] = $this->request->post['billing_apt'];
        } elseif (!empty($billing_address_info)) {
            $dataEdit['billing_apt'] = $billing_address_info['apt_number'];
        } else {
            $dataEdit['billing_apt'] = '';
        }

        if (isset($this->request->post['billing_postcode'])) {
            $dataEdit['billing_postcode'] = $this->request->post['billing_postcode'];
        } elseif (!empty($billing_address_info)) {
            $dataEdit['billing_postcode'] = $billing_address_info['postcode'];
        } else {
            $dataEdit['billing_postcode'] = '';
        }

        if (isset($this->request->post['billing_city'])) {
            $dataEdit['billing_city'] = $this->request->post['billing_city'];
        } elseif (!empty($billing_address_info)) {
            $dataEdit['billing_city'] = $billing_address_info['city'];
        } else {
            $dataEdit['billing_city'] = '';
        }

        if (isset($this->request->post['billing_country_id'])) {
            $dataEdit['billing_country_id'] = (int)$this->request->post['billing_country_id'];
        }  elseif (!empty($billing_address_info)) {
            $dataEdit['billing_country_id'] = $billing_address_info['country_id'];
        } else {
            $dataEdit['billing_country_id'] = $this->config->get('config_country_id');
        }

        if (isset($this->request->post['billing_zone_id'])) {
            $dataEdit['billing_zone_id'] = (int)$this->request->post['billing_zone_id'];
        }  elseif (!empty($billing_address_info)) {
            $dataEdit['billing_zone_id'] = $billing_address_info['zone_id'];
        } else {
            $dataEdit['billing_zone_id'] = '';
        }

        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');

        $dataEdit['countries'] = $this->model_localisation_country->getCountries();
        $dataEdit['general_states'] = $this->model_localisation_zone->getZonesByCountryId($dataEdit['general_country_id']);
        $dataEdit['billing_states'] = $this->model_localisation_zone->getZonesByCountryId($dataEdit['billing_country_id']);

        return $this->load->view('account/edit',$dataEdit);

    }

    protected function validate() {
        if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
            $this->error['firstname'] = $this->language->get('error_firstname');
        }

        if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
            $this->error['lastname'] = $this->language->get('error_lastname');
        }

        if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
            $this->error['email'] = $this->language->get('error_email');
        }

        if (($this->customer->getEmail() != $this->request->post['email']) && $this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
            $this->error['warning'] = $this->language->get('error_exists');
        }


        if ((utf8_strlen(trim($this->request->post['general_address'])) < 3) || (utf8_strlen(trim($this->request->post['general_address'])) > 128)) {
            $this->error['general_address'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen(trim($this->request->post['general_city'])) < 2) || (utf8_strlen(trim($this->request->post['general_city'])) > 128)) {
            $this->error['general_city'] = $this->language->get('error_city');
        }

        $this->load->model('localisation/country');

        $country_info = $this->model_localisation_country->getCountry($this->request->post['general_country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['general_postcode'])) < 2 || utf8_strlen(trim($this->request->post['general_postcode'])) > 10)) {
            $this->error['general_postcode'] = $this->language->get('error_postcode');
        }

        if ($this->request->post['general_country_id'] == '' || !is_numeric($this->request->post['general_country_id'])) {
            $this->error['general_country'] = $this->language->get('error_country');
        }

        if (!isset($this->request->post['general_zone_id']) || $this->request->post['general_zone_id'] == '' || !is_numeric($this->request->post['general_zone_id'])) {
            $this->error['general_zone'] = $this->language->get('error_zone');
        }

        if ((utf8_strlen(trim($this->request->post['billing_address'])) < 3) || (utf8_strlen(trim($this->request->post['billing_address'])) > 128)) {
            $this->error['billing_address'] = $this->language->get('error_address_1');
        }

        if ((utf8_strlen(trim($this->request->post['billing_city'])) < 2) || (utf8_strlen(trim($this->request->post['billing_city'])) > 128)) {
            $this->error['billing_city'] = $this->language->get('error_city');
        }

        $country_info = $this->model_localisation_country->getCountry($this->request->post['billing_country_id']);

        if ($country_info && $country_info['postcode_required'] && (utf8_strlen(trim($this->request->post['billing_postcode'])) < 2 || utf8_strlen(trim($this->request->post['billing_postcode'])) > 10)) {
            $this->error['billing_postcode'] = $this->language->get('error_postcode');
        }

        if ($this->request->post['billing_country_id'] == '' || !is_numeric($this->request->post['billing_country_id'])) {
            $this->error['billing_country'] = $this->language->get('error_country');
        }

        if (!isset($this->request->post['billing_zone_id']) || $this->request->post['billing_zone_id'] == '' || !is_numeric($this->request->post['billing_zone_id'])) {
            $this->error['billing_zone'] = $this->language->get('error_zone');
        }

        $this->load->model('account/customer');

        if(utf8_strlen($this->request->post['old_password']) > 0 || utf8_strlen($this->request->post['password']) > 0 ) {

            if (!isset($this->request->post['old_password']) || !$this->model_account_customer->validateCustomer($this->customer->getId(), $this->request->post['old_password'])) {
                $this->error['old_password'] = $this->language->get('error_password');
            }

            if ((utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) < 4) || (utf8_strlen(html_entity_decode($this->request->post['password'], ENT_QUOTES, 'UTF-8')) > 40)) {
                $this->error['password'] = $this->language->get('error_password');
            }

            if ($this->request->post['confirm'] != $this->request->post['password']) {
                $this->error['confirm'] = $this->language->get('error_confirm');
            }
        }
        // Custom field validation
        $this->load->model('account/custom_field');

        $custom_fields = $this->model_account_custom_field->getCustomFields('account', $this->config->get('config_customer_group_id'));

        foreach ($custom_fields as $custom_field) {
            if ($custom_field['location'] == 'account') {
                if ($custom_field['required'] && empty($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']])) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                } elseif (($custom_field['type'] == 'text') && !empty($custom_field['validation']) && !filter_var($this->request->post['custom_field'][$custom_field['location']][$custom_field['custom_field_id']], FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => $custom_field['validation'])))) {
                    $this->error['custom_field'][$custom_field['custom_field_id']] = sprintf($this->language->get('error_custom_field'), $custom_field['name']);
                }
            }
        }

        return !$this->error;
    }
}
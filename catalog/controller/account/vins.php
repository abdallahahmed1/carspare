<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 13/08/18
 * Time: 11:26 ص
 */

class ControllerAccountVins extends Controller
{
    public function index(){

        $this->load->model('account/vins');
        $data = array();
        $data['vins'] = $this->model_account_vins->getVins($this->customer->getId());
        $data['action'] = $this->url->link('account/vins/add','',true);
        return $this->load->view('account/my_vins', $data);
    }

    public function add(){

        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/account', '', true);

            $this->response->redirect($this->url->link('account/register', '', true));
        }
        $this->load->model('account/vins');

        $data = $this->request->post;
        foreach($data as $name => $vin){
            if(is_numeric($name)){
                if(strlen($vin) == 0){
                    $this->model_account_vins->removeVin($name);
                }
                else if(strlen($vin) == 17){
                    $this->model_account_vins->updateVin($name, $vin);
                }
            }
            else if(strlen($vin) == 17){
                $this->model_account_vins->addVin($this->customer->getId(), $vin);
            }
        }

        $this->response->redirect($this->url->link('account/account', 'tab=vins', true));


    }


}
<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 16/08/18
 * Time: 11:50 ص
 */

$_['text_no_agents']   = 'There are no agents available right now to take your call. Please leave a message and we will reply by email.';
$_['text_submit']      = 'SEND';
$_['text_success']     = 'Your message has been successfully sent to the store owner!';
$_['heading_title']     = 'Leave a message';

$_['entry_name']     = 'Name';
$_['entry_email']    = 'Email Address';
$_['entry_message']  = 'Message';

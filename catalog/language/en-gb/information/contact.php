<?php
// Heading
$_['heading_title']  = 'CONTACT US';

// Text
$_['text_location']  = 'OUR LOCATION';
$_['text_description']  = 'Please complete the following form and we will reach out to you as soon as possible to discuss how we can help';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Address';
$_['text_telephone'] = 'Phone No.';
$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comment';
$_['text_vin']       = 'VIN No';
$_['text_item']       = 'Item needed';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry
$_['entry_name']     = 'Name';
$_['entry_email']    = 'Email Address';
$_['entry_enquiry']  = 'Enquiry';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
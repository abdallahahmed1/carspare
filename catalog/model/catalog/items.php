<?php

class ModelCatalogItems extends Model{

    public function getAccessories($filter){
        $car_id = $filter['car_id'];
        $q = "Select *,(SELECT price FROM "
            . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '"
            . (int)$this->config->get('config_customer_group_id')
            . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special from ".DB_PREFIX."product_description pd left join ".DB_PREFIX."product p
               on pd.product_id = p.product_id ";

        if(isset($filter['edition_id'])){
            $q .= " join ".DB_PREFIX."product_edition pe on p.product_id = pe.product_id ";
        }

        if(isset($filter['category_id'])){
            $q .= "join ".DB_PREFIX."product_to_category pc on pd.product_id = pc.product_id ";
        }
            $q .= "where ptype = 'accessories' and car_id = ".(int)$car_id." ";

        if(isset($filter['edition_id'])){
            $q .= " and pe.filter_id = ".$filter['edition_id']." and pe.product_id = p.product_id ";
        }

        if(isset($filter['category_id'])){
            $q .= "and pc.category_id = ".$filter['category_id']." ";
        }
        $sort = 'asc';
        if(isset($filter['price'])){
            $sort = $filter['price'];
        }
            $q .= "ORDER BY p.price $sort ";
        return $this->db->query($q)->rows;
    }

    public function getAccessoriesCategories($car_id){



            $q = "select DISTINCT cd.* from ".DB_PREFIX."product_description pd 
              JOIN ".DB_PREFIX."product_to_category pc on pd.product_id = pc.product_id join ".DB_PREFIX."category_description cd 
              on cd.category_id = pc.category_id join ".DB_PREFIX."category_filter cf on pc.category_id = cf.category_id 
              JOIN ".DB_PREFIX."filter_description fd on fd.filter_id = cf.filter_id  where fd.name = 'Accessories' and pd.car_id = ".(int)$car_id ;


        return $this->db->query($q)->rows;
    }

    public function getPartsCategories($car_id, $edition_id = false){
        if($edition_id)
            $q = "select DISTINCT cd.*, c.image from ".DB_PREFIX."product_description pd 
              JOIN ".DB_PREFIX."product_to_category pc on pd.product_id = pc.product_id join ".DB_PREFIX."category_description cd 
              on cd.category_id = pc.category_id join ".DB_PREFIX."category c on cd.category_id = c.category_id join ".DB_PREFIX."category_filter cf on pc.category_id = cf.category_id 
              JOIN ".DB_PREFIX."filter_description fd on fd.filter_id = cf.filter_id join cs_product_edition pe 
              on pe.product_id = pd.product_id where fd.name = 'Parts' and pd.car_id = ".(int)$car_id ." and pe.filter_id = ".$edition_id;
        else
            $q = "select DISTINCT cd.*, c.image from ".DB_PREFIX."product_description pd 
              JOIN ".DB_PREFIX."product_to_category pc on pd.product_id = pc.product_id join ".DB_PREFIX."category_description cd 
              on cd.category_id = pc.category_id join ".DB_PREFIX."category c on cd.category_id = c.category_id join ".DB_PREFIX."category_filter cf on pc.category_id = cf.category_id 
              JOIN ".DB_PREFIX."filter_description fd on fd.filter_id = cf.filter_id  where fd.name = 'Parts' and pd.car_id = ".(int)$car_id ;

        $results = $this->db->query($q)->rows;

        $categories = array();
        foreach ($results as $category){

            $c = $this->db->query("select distinct pd.* from ".DB_PREFIX."product_description pd join ".DB_PREFIX."product_to_category p2c 
                                    on pd.product_id = p2c.product_id where pd.top = 1 and pd.car_id = ".(int)$car_id." and p2c.category_id = ".$category['category_id']." limit 5")->rows;
            $top_parts = array();
            foreach ($c as $child){
                $top_parts[] = array(
                    'name' => $child['name'],
                    'part_id' => $child['product_id']
                );
            }
            if(!$top_parts) continue;
            $categories[] = array(
                'name' => $category['name'],
                'category_id' => $category['category_id'],
                'image' => $category['image'],
                'top_parts' => $top_parts
            );
        }
        return $categories;
    }

    public function getTopParts($car_id, $edition_id, $category_id){
        $q = $this->db->query("Select distinct pd.*, p.* from ".DB_PREFIX."product p join ".DB_PREFIX."product_description pd on pd.product_id = p.product_id join 
                ".DB_PREFIX."product_edition pe on pe.filter_id = ".(int)$edition_id." join ".DB_PREFIX."product_to_category p2c 
                                    on pd.product_id = p2c.product_id where p2c.category_id = ".(int)$category_id." and pd.car_id = ".(int)$car_id." and pd.top = 1 and pd.ptype = 'parts' order by pd.name asc")->rows;

        $parts = array();
        foreach ($q as $part){
            $parts[] = array(
                'name' => $part['name'],
                'product_id' => $part['product_id'],
                'image' => $part['image'],
            );
        }
        return $parts;
    }

    public function getTopPart($part_id){
        //Illustrators
        $this->load->model('catalog/product');
        $illustrators = $this->model_catalog_product->getProductRelated($part_id);

        //Children
        $results = $this->db->query("select distinct m.name as manufacturer, p.*, pd.*, pc.value, (SELECT price FROM "
            . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '"
            . (int)$this->config->get('config_customer_group_id')
            . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special from ".DB_PREFIX."product_description pd join
                                     ".DB_PREFIX."product p on p.product_id = pd.product_id join ".DB_PREFIX."manufacturer m on p.manufacturer_id = m.manufacturer_id join ".DB_PREFIX."parts_children pc on 
                                      pd.product_id = pc.child_id where pc.product_id = $part_id")->rows;
        $children = array();

        foreach ($results as $child){
            $children[] = array(
                'name' => $child['name'],
                'value' => $child['value'],
                'product_id' => $child['product_id'],
                'mpn'   => $child['mpn'],
                'price' => $child['price'],
                'manufacturer' => $child['manufacturer'],
                'image' => $child['image'],
                'special' => $child['special'],
                'tax_class_id' => $child['tax_class_id']
            );
        }
        $product = $this->model_catalog_product->getProduct($part_id);
        $q = $this->db->query("Select * from ".DB_PREFIX."product_edition where product_id = $part_id")->rows;
        if(count($q ) > 0)
            $product['edition_id'] = $q[0]['filter_id'];
        $data = array();
        $data['illustrators'] = $illustrators;
        $data['children'] = $children;
        $data['part'] = $product;

        return $data;
    }

    public function getPartParent($part_id){
        $q = $this->db->query("Select product_id from ".DB_PREFIX."parts_children pc where child_id = ".(int)$part_id)->rows;

        return $q[0]['product_id'];
    }

}
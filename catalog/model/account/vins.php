<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 13/08/18
 * Time: 10:39 ص
 */

class ModelAccountVins extends Model
{
    public function addVin($customer_id,$vin)
    {
        $query_db = "INSERT INTO ".DB_PREFIX."customer_vins(customer_id, vin) VALUES(".(int)$customer_id.",'$vin')";
        $this->db->query($query_db);
    }

    public function removeVin($id)
    {
        $query_db = "DELETE FROM ".DB_PREFIX."customer_vins WHERE id = ".(int)$id;
        $this->db->query($query_db);
    }

    public function updateVin($id,  $vin)
    {
        $query_db = "Update " . DB_PREFIX . "customer_vins SET vin = $vin WHERE id = ".(int)$id;
        $this->db->query($query_db);
    }

    public function getVins($customer_id){
        $query_db = "SELECT * FROM ".DB_PREFIX."customer_vins WHERE customer_id = ".(int)$customer_id;
        $query = $this->db->query($query_db);
        return $query->rows;
    }

}
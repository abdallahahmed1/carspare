<?php
/**
 * Created by PhpStorm.
 * User: andrewemad
 * Date: 12/08/18
 * Time: 05:58 م
 */

class ModelAccountGarage extends Model
{
    public function getGarage($customer_id){
        $query_db = "SELECT pd.product_id as product_id , pd.name as name, m.name as manufacturer, pd.year,pd.product_id as id FROM "
            .DB_PREFIX. "customer_garage cg INNER JOIN "
            .DB_PREFIX. "product_description pd ON cg.product_id = pd.product_id INNER JOIN "
            .DB_PREFIX. "product p ON cg.product_id = p.product_id INNER JOIN "
            .DB_PREFIX. "manufacturer m ON m.manufacturer_id = p.manufacturer_id WHERE cg.customer_id = $customer_id";

        $query = $this->db->query($query_db);
        return $query->rows;
    }

    public function addToGarage($customer_id, $product_id){
        $query_db = "INSERT INTO ".DB_PREFIX."customer_garage VALUES(".(int)$customer_id.", ".(int)$product_id.")";
        $query = $this->db->query($query_db);
    }

    public function removeFromGarage($customer_id, $product_id){
        $query_db = "DELETE FROM ".DB_PREFIX."customer_garage WHERE customer_id = ".(int)$customer_id." AND product_id = ".(int)$product_id;
        $query = $this->db->query($query_db);

    }


}